/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       IoHwAb_ChannelsCfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines the sensor and actuator channels on the ECU abstraction level for accessing all the ECU
 *                signals.
 *
 *    From the MCAL drivers' perspective the channels do not contain the information about the context they're used in.
 *    In this layer the MCAL channels are mapped to ECU signal names that describe the signals origin or destination
 *    that are correlated with a sensor or an actuator.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef IOHWAB_CHANNELSCFG_H
#define IOHWAB_CHANNELSCFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "IoHwAb_ApiCfg.h"

#if (STD_ON == IOHWAB_API)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/* Redefine all the MCAL driver channels by providing the information about the used peripherals. */

/* -------------------------- Analog ---------------------------*/

#if (STD_ON == IOHWAB_ANALOG_API)

#define IOHWAB_ANALOG_POTENTIOMETER                (ADC_C_5)

#endif /* (STD_ON == IOHWAB_ANALOG_API) */

/* -------------------------- Digital --------------------------*/

#if (STD_ON == IOHWAB_DIGITAL_API)

#define IOHWAB_DIGITAL_DISP7SEG_LEFT_DIGIT         (DIO_A)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_DIGIT        (DIO_B)

#define IOHWAB_DIGITAL_DISP7SEG_LEFT_A             (DIO_A_0)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_B             (DIO_A_1)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_C             (DIO_A_2)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_D             (DIO_A_3)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_E             (DIO_A_4)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_F             (DIO_A_5)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_G             (DIO_A_6)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_DP1           (DIO_A_7)
#define IOHWAB_DIGITAL_DISP7SEG_LEFT_DIG1          (DIO_A_15)

#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_A            (DIO_B_0)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_B            (DIO_B_1)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_C            (DIO_B_2)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_D            (DIO_B_3)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_E            (DIO_B_4)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_F            (DIO_B_5)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_G            (DIO_B_6)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_DP2          (DIO_B_7)
#define IOHWAB_DIGITAL_DISP7SEG_RIGHT_DIG2         (DIO_B_15)

#define IOHWAB_DIGITAL_BTN_LEFT                    (DIO_D_0)
#define IOHWAB_DIGITAL_BTN_RIGHT                   (DIO_D_1)
#define IOHWAB_DIGITAL_BTN_HAZARD                  (DIO_D_2)

#endif /* (STD_ON == IOHWAB_DIGITAL_API) */

/* ---------------------------- PWM ----------------------------*/

#if (STD_ON == IOHWAB_PWM_API)

#define IOHWAB_PWM_MOTOR_FAN (PWM_A_1)

#endif /* (STD_ON == IOHWAB_PWM_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#endif /* (STD_ON == IOHWAB_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* IOHWAB_CHANNELSCFG_H */
