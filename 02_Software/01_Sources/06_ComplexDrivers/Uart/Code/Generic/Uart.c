/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the Uart_gv_Init() and Uart_gv_Transmit() functions.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Uart.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Local variable used to store the address of the channel configuration table. */
static Uart_ChannelConfigType *Uart_t_ChannelConfig;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes the UART module using the register values from the configuration table.
 * \param      pt_Config: Pointer to the configuration table.
 * \return     -
 */
void Uart_gv_Init(const Uart_ConfigType * pt_Config)
{
   Uart_t_ChannelConfig = (Uart_ChannelConfigType*) (pt_Config->kpt_ChannelConfig);
   RegInit_gv_Masked32Bits(pt_Config->kpt_InitConfig);
}

/* Disable USART Transmit interfaces if the Transmit API is OFF. */
#if (UART_TRANSMIT_API == STD_ON)

/**
 * \brief      Returns the status of the transmitter channel.
 * \param      t_ChannelId: Numeric ID of the transmitter channel.
 * \return     UART_TX_READY if the transmission was completed; UART_TX_BUSY if the transmission is not complete.
 */
Uart_TxStatusType Uart_gt_GetTransmitStatus(Uart_ChannelType t_ChannelId)
{
   Uart_TxStatusType t_ReturnVal;
   if ((*Uart_t_ChannelConfig[t_ChannelId].pt_UsartStatusReg & USART_SR_TC) != FALSE)
   {
      t_ReturnVal = UART_TX_READY;
   }
   else
   {
      t_ReturnVal = UART_TX_BUSY;
   }
   return t_ReturnVal;
}

/**
 * \brief      Sends the byte located at the address of *puc_TransmitAddr.
 * \param      t_ChannelId: Numeric ID of the transmitter channel.
 * \param      puc_Transmit: Address of the payload to be transmitted.
 * \return     -
 */
void Uart_gv_Transmit(Uart_ChannelType t_ChannelId, uint8 * puc_TransmitAddr)
{
   *Uart_t_ChannelConfig[t_ChannelId].pt_UsartDataReg = *puc_TransmitAddr;
}

#endif /* USART_TRANSMIT_API */

/* Disable USART Receive interfaces if the Receive API is OFF. */
#if (UART_RECEIVE_API == STD_ON)

/**
 * \brief      Returns the status of the receiver channel.
 * \param      t_ChannelId: Numeric ID of the receiver channel.
 * \return     UART_RX_READY if the data is ready to be read; UART_RX_NO_DATA if there is no data received at the
 *             moment of interrogation.
 */
Uart_RxStatusType Uart_gt_GetReceiveStatus(Uart_ChannelType t_ChannelId)
{
   Uart_TxStatusType t_ReturnVal;
   if ((*Uart_t_ChannelConfig[t_ChannelId].pt_UsartStatusReg & USART_SR_RXNE) != FALSE)
   {
      t_ReturnVal = UART_RX_READY;
   }
   else
   {
      t_ReturnVal = UART_RX_NO_DATA;
   }
   return t_ReturnVal;
}

/**
 * \brief      Sends the byte located at the address of *puc_TransmitAddr.
 * \param      t_ChannelId: Numeric ID of the transmitter channel.
 * \param      puc_ReceiveAddr: Address of the variable to store the received byte.
 * \return     -
 */
void Uart_gv_Receive(Uart_ChannelType t_ChannelId, uint8 * puc_ReceiveAddr)
{

   *puc_ReceiveAddr = *Uart_t_ChannelConfig[t_ChannelId].pt_UsartDataReg;
}

#endif /* UART_RECEIVE_API */

