/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Exports all the necessary data types and functions to be used by the Uart module.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef UART_H
#define UART_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Uart_Cfg.h"
#include "RegInit.h"
#include "stm32f407xx.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief ID of an UART channel. */
typedef uint8 Uart_ChannelType;
/** \brief Init configuration type. */
typedef RegInit_Masked32BitsConfigType Uart_InitConfigType;
/** \brief Channel configuration data type. */
typedef struct
{
   /** \brief Pointer to the data register of the channel. */
   uint32* pt_UsartDataReg;
   /** \brief Pointer to the status register of the channel. */
   uint32* pt_UsartStatusReg;

} Uart_ChannelConfigType;

/** \brief Enum type defining the status of TX channel. */
typedef enum
{
   /** \brief Transmission is not complete. */
   UART_TX_BUSY,
   /** \brief Transmission is complete. */
   UART_TX_READY,
} Uart_TxStatusType;

/** \brief Enum type defining the status of RX channel. */
typedef enum
{
   /** \brief No data received. */
   UART_RX_NO_DATA,
   /** \brief Data received and ready to be read. */
   UART_RX_READY,
} Uart_RxStatusType;

/** \brief Defines the global UART configuration type. */
typedef struct
{
   /** \brief Pointer to the initialization config. */
   const Uart_InitConfigType *kpt_InitConfig;
   /** \brief Pointer to the channel config. */
   const Uart_ChannelConfigType *kpt_ChannelConfig;
} Uart_ConfigType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Uart_ConfigType Uart_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Uart_gv_Init(const Uart_ConfigType * pt_Config);

#if (UART_TRANSMIT_API == STD_ON)
extern Uart_TxStatusType Uart_gt_GetTransmitStatus(Uart_ChannelType t_ChannelId);
extern void Uart_gv_Transmit(Uart_ChannelType t_ChannelId, uint8 * puc_TransmitAddr);
#endif

#if (UART_RECEIVE_API == STD_ON)
extern Uart_RxStatusType Uart_gt_GetReceiveStatus(Uart_ChannelType t_ChannelId);
extern void Uart_gv_Receive(Uart_ChannelType t_ChannelId, uint8 * puc_ReceiveAddr);
#endif

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* UART_H */
