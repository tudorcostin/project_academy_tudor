/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Uart_PBcfg.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the configuration tables for the initialization of the Uart module.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Uart.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/** \brief UART Number of channels. */
#define UART_NUMBER_OF_CHANNELS (2U)
/** \brief UART Number of registers to be initialized. */
#define UART_NUMBER_OF_REGISTERS (6U)
/** \brief USART1 Fraction part of USARTDIV. */
#define UART1_DIV_FRACTION (8U)
/** \brief USART1 Mantissa part of USARTDIV. */
#define UART1_DIV_MANTISSA ((19U)<<4)
/** \brief UART1 Baudrate of 115200 */
#define UART1_BAUD (694U)
/** \brief Define the baudrate for the Uart to 9600 bps.*/
#define UART3_ONEWIRE_BAUDRATE ( 40000000U / 9600U )

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief UART register configuration table. */
static const RegInit_Masked32BitsSingleType Uart_kat_RegConfig[UART_NUMBER_OF_REGISTERS] =
   {
      /* Enable UART1 Transmitter and Receiver. */
      {
         &USART1->CR1,
         ~(USART_CR1_RE |
         USART_CR1_TE),
         (USART_CR1_RE |
         USART_CR1_TE)
      },
      /* Set the baud rate to 256000. */
      {
         &USART1->BRR,
         ~(UART1_DIV_FRACTION |
         UART1_DIV_MANTISSA),
         (UART1_BAUD)
      },
      /* Start UART1. */
      {
         &USART1->CR1,
         ~(USART_CR1_UE),
         (USART_CR1_UE)
      },
   };

/** \brief Channel configuration table for each UART Channel. */
static const Uart_ChannelConfigType Uart_kat_ChannelConfig[UART_NUMBER_OF_CHANNELS] =
   {
      { (uint32*) &USART1->DR, (uint32*) &USART1->SR },
      { (uint32*) &USART1->DR, (uint32*) &USART1->SR },
   };

/** \brief Defines the configuration table for the registers and the number of registers to be configured. */
static const Uart_InitConfigType Uart_kt_InitConfig =
   { Uart_kat_RegConfig, UART_NUMBER_OF_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Exports the global initialization table. */
const Uart_ConfigType Uart_gkt_Config =
   { &Uart_kt_InitConfig, Uart_kat_ChannelConfig };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

