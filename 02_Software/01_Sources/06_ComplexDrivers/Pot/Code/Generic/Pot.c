/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Servo.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the necessary functions for aquiring the data from a potentiometer.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pot.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief The ADC Channel assigned to the potentiometer. */
#define POT_ADC_CHANNEL (ADC_A_4)
/** \brief Maximum value of the conversion for a 12 bit ADC. */
#define POT_ADC_MAX_VAL (4095U)
/** \brief Maximum speed in Km/h. */
#define POT_SPEED_MAX_VAL (140U)
/** \brief Absolute value of the ADC converted to 1 degree calculated with formula:
 * Max ADC Raw value / Max speed value 4095 / 140 */
#define POT_ADC_TO_DEGREE ((POT_ADC_MAX_VAL)/POT_SPEED_MAX_VAL)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Local variable that stores the raw values from the ADC conversion. */
static uint16 Pot_us_AdcRawValue;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static uint16 Pot_AbsDiff(uint16 us_FirstParam, uint16 us_SecondParam);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Calculates the absolute difference between the 2 given parameters.
 * \param      us_FirstParam
 * \param      us_SecondParam
 * \return     The absolute difference.
 */
static uint16 Pot_AbsDiff(uint16 us_FirstParam, uint16 us_SecondParam)
{
   uint16 us_Return;
   /* Check to see which parameter is bigger. */
   if (us_FirstParam < us_SecondParam)
   {
      /* Return the difference between the second parameter and the first parameter. */
      us_Return = us_SecondParam - us_FirstParam;
   }
   else if (us_SecondParam < us_FirstParam)
   {
      /* Return the difference between the first parameter and the second parameter. */
      us_Return = us_FirstParam - us_SecondParam;
   }
   else
   {
      /* Both parameters are equal. */
      us_Return = 0;
   }
   return us_Return;
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes the Pot module.
 * \param      -
 * \return     -
 */
void Pot_Init()
{
   /* Sets up the variable that stores the ADC conversion for channel group ADC_A_1 */
   Adc_SetupResultBuffer(POT_ADC_CHANNEL, &Pot_us_AdcRawValue);
}


/**
 * \brief      Gets the value of the potentiometer on a range of 0 to 140.
 * \param      -
 * \return     The converted value from the potentiometer
 *             Range: 0 - 140.
 */
uint16 Pot_GetSpeed()
{
   static uint16 AverageSpeed = 0;
   static uint16 LastAverageSpeed = 0;
   static uint8 AverageCount = 0;
   Adc_StartGroupConversion(POT_ADC_CHANNEL);
   if (Adc_GetGroupStatus(POT_ADC_CHANNEL) == ADC_COMPLETED)
   {
      /* Average 10 values to flatten the noise generated by the imperfections in the electrical circuit. */
      if (AverageCount < 10)
      {
         /* Speed range: 0-140; ADC Range 0-4095 -> ADC Value of 29 corelates to 1 Km/h.  */
         AverageSpeed += (Pot_us_AdcRawValue / POT_ADC_TO_DEGREE);
         AverageCount++;
      }
      else if (AverageCount == 10)
      {
         /* Average the last 10 measurements. */
         AverageSpeed /= 10;
         /* Set the angle. */
         if (Pot_AbsDiff(AverageSpeed, LastAverageSpeed) >= 2)
         {
            /* Retain the present angle. */
            LastAverageSpeed = AverageSpeed;
         }
         else
         {
            /* The difference is negligible - Do nothing. */
         }
         /* Reset the used variables. */
         AverageCount = 0;
         AverageSpeed = 0;
      }
   }
   else
   {
      /* Conversion not done. */
   }
   /* Return the stable value of the potentiometer. */
   return LastAverageSpeed;
}
