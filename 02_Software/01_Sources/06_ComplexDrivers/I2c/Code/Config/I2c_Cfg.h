/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       I2c_Cfg.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Defines and exports the number of I2C channels, their IDs and defines the error detection API
 *                parameters.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef I2C_CFG_H
#define I2C_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Activates or deactivates the API for detecting and reporting protocol and bus off errors. */
#define I2C_ERROR_DETECTION_API           (STD_ON)

/** \brief  Periodicity of the main function expressed in microseconds. */
#define I2C_MAIN_PERIODICITY_US           (70U)

#if (STD_ON == I2C_ERROR_DETECTION_API)

/** \brief  Timeout window for setting a protocol error. Must be a multiple of the main function periodicity. */
#define I2C_ERROR_TIMEOUT_PROTOCOL_US     (350U)

/** \brief  Timeout window for setting a bus off error. Must be a multiple of the protocol error timeout window. */
#define I2C_ERROR_TIMEOUT_BUS_OFF_US      (10500U)

#endif

/** \brief  The number of I2C channels that are used in the configuration (a maximum number of 3 in case all I2C
 *          peripherals are used). */
#define I2C_NUMBER_OF_CHANNELS            (2U)

/** \brief  ID of the channel using the I2C1 peripheral which controls port B pin 8 as SCL and pin 9 as SDA. */
#define I2C_B_SCL_8_SDA_9                 (0U)

/** \brief  ID of the channel using the I2C1 peripheral which controls port B pin 10 as SCL and pin 11 as SDA. */
#define I2C_B_SCL_10_SDA_11               (1U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* I2C_CFG_H */
