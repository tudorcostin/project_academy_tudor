/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       I2c_PBcfg.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Defines the global configuration containing the initialization configurations and hardware units of
 *                all the I2C channels.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "I2c.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Total number of registers to be loaded for the I2C1 channel. */
#define I2C_I2C1_NUMBER_OF_INIT_REGISTERS    (2U)

/** \brief  Total number of registers to be loaded for the I2C2 channel. */
#define I2C_I2C2_NUMBER_OF_INIT_REGISTERS    (2U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------- I2C1 Configuration ------------------------------------------------*/

/** \brief  Stores the configuration of all the registers to be set for the I2C1 channel. */
static const RegInit_Masked32BitsSingleType I2c_kat_I2c1InitLoadRegisters[I2C_I2C1_NUMBER_OF_INIT_REGISTERS] =
   {
      /* Peripheral clock frequency (TPCLK) set to 40 MHz (derived from APB1 clock). */
      {
         (volatile uint32*) &I2C1->CR2,

         (uint32) ~(I2C_CR2_FREQ),

         (uint32) (40U)
      },

      /* Set clock control register to standard mode (100 Kbps baud rate):
       *    TSCL = THigh + TLow = 2 * CCR * TPCLK
       *    CCR = TSCL / (2 * TPCLK)
       *    CCR = 10000 ns / (2 * 25 ns)
       *    CCR = 200
       *
       *    F/S = 0. */
      {
         &I2C1->CCR,

         (uint32) ~(
         I2C_CCR_FS |
         I2C_CCR_CCR),

         (uint32) (200U)
      },
   };

/*----------------------------------------------- I2C2 Configuration ------------------------------------------------*/

/** \brief  Stores the configuration of all the registers to be set for the I2C1 channel. */
static const RegInit_Masked32BitsSingleType I2c_kat_I2c2InitLoadRegisters[I2C_I2C2_NUMBER_OF_INIT_REGISTERS] =
   {
      /* Peripheral clock frequency (TPCLK) set to 40 MHz (derived from APB1 clock). */
      {
         (volatile uint32*) &I2C2->CR2,

         (uint32) ~(I2C_CR2_FREQ),

         (uint32) (40U)
      },

      /* Set clock control register to standard mode (100 Kbps baud rate):
       *    TSCL = THigh + TLow = 2 * CCR * TPCLK
       *    CCR = TSCL / (2 * TPCLK)
       *    CCR = 10000 ns / (2 * 25 ns)
       *    CCR = 200
       *
       *    F/S = 0. */
      {
         &I2C2->CCR,

         (uint32) ~(
         I2C_CCR_FS |
         I2C_CCR_CCR),

         (uint32) (200U)
      },
   };

/*----------------------------------------------- I2C2 Configuration ------------------------------------------------*/

/*----------------------------------------------- I2C3 Configuration ------------------------------------------------*/

/*------------------------------------------- All Channels Configuration --------------------------------------------*/

/** \brief  Stores the configuration of all I2C channels. */
static const I2c_ChannelConfigType I2c_kat_ChannelsConfig[I2C_NUMBER_OF_CHANNELS] =
   {
      /* I2C_B_SCL_8_SDA_9 */
      {
         {
            I2c_kat_I2c1InitLoadRegisters,
            I2C_I2C1_NUMBER_OF_INIT_REGISTERS
         },

         (I2C1),
      },

      /* I2C_B_SCL_10_SDA_11 */
      {
         {
            I2c_kat_I2c2InitLoadRegisters,
            I2C_I2C2_NUMBER_OF_INIT_REGISTERS
         },

         (I2C2),
      },
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Post-build configuration. References the array containing all the I2C channels. */
const I2c_ConfigType I2c_gkt_Config = (const I2c_ConfigType) I2c_kat_ChannelsConfig;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

