/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       I2c.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Declares the I2C data types, exports the global post build configuration and exports the interfaces
 *                for controlling all the I2C channels.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef I2C_H
#define I2C_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "I2c_Cfg.h"

#include "RegInit.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Holds the numeric IDs of all I2C channels. The IDs are zero based. */
typedef uint8 I2c_ChannelType;

/** \brief  Defines the status of an I2C channel. */
typedef enum
{
   /** \brief  The channel is waiting for a request. */
   I2C_STATUS_IDLE,

   /** \brief  The channel has successfully completed the last request. The status is immediately switched to the idle
    *          state after being read. */
   I2C_STATUS_COMPLETED,

   /** \brief  The channel is currently processing a request. */
   I2C_STATUS_BUSY,

   /** \brief  The channel recognized a deviation from the protocol while processing a frame or recognized a busy
    *          condition when there was no ongoing request. */
   I2C_STATUS_ERROR_PROTOCOL,

   /** \brief  The channel recognized too many successive protocol errors. The status is reset only when a new request
    *          is successfully completed. */
   I2C_STATUS_ERROR_BUS_OFF,

} I2c_StatusType;

/** \brief  Configuration of an I2C channel consisting of the initialization configuration and the used hardware unit. */
typedef struct
{
   /** \brief  Configuration to be loaded in the initialization function and when resetting the channel. */
   RegInit_Masked32BitsConfigType t_InitConfig;

   /** \brief  Base address of the I2C peripheral hardware unit. */
   I2C_TypeDef* pt_HardwareUnit;

} I2c_ChannelConfigType;

/** \brief  Global I2C configuration type containing the configuration of all the channels. */
typedef I2c_ChannelConfigType* I2c_ConfigType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const I2c_ConfigType I2c_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void I2c_gv_Init(const I2c_ConfigType * pt_Config);

extern void I2c_gv_Main(void);

extern I2c_StatusType I2c_gt_GetStatus(I2c_ChannelType t_ChannelId);

extern void I2c_gv_Wakeup(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr);

extern void I2c_gv_Write(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength);

extern void I2c_gv_Read(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* I2C_H */
