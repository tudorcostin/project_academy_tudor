/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       I2c.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements all the I2C control interfaces. The user has to use the get status interface before
 *                making any request. Each channel defines its own state machine for keeping track of all the different
 *                states happening during a request.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "I2c.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == I2C_ERROR_DETECTION_API)

#if (((I2C_ERROR_TIMEOUT_PROTOCOL_US / I2C_MAIN_PERIODICITY_US) * I2C_MAIN_PERIODICITY_US) \
      == I2C_ERROR_TIMEOUT_PROTOCOL_US)

/** \brief  Defines the counting threshold for setting up a protocol error. */
#define I2C_ERROR_TIMEOUT_PROTOCOL_COUNT (I2C_ERROR_TIMEOUT_PROTOCOL_US / I2C_MAIN_PERIODICITY_US)

#else

#error "Invalid configuration. I2C_ERROR_TIMEOUT_PROTOCOL_US shall be a multiple of I2C_MAIN_PERIODICITY_US."

#endif

#if (((I2C_ERROR_TIMEOUT_BUS_OFF_US / I2C_ERROR_TIMEOUT_PROTOCOL_US) * I2C_ERROR_TIMEOUT_PROTOCOL_US) \
      == I2C_ERROR_TIMEOUT_BUS_OFF_US)

/** \brief  Defines the counting threshold for setting up a bus off error. */
#define I2C_ERROR_TIMEOUT_BUS_OFF_COUNT (I2C_ERROR_TIMEOUT_BUS_OFF_US / I2C_ERROR_TIMEOUT_PROTOCOL_US)

#else

#error "Invalid configuration. I2C_ERROR_TIMEOUT_PROTOCOL_US shall be a multiple of I2C_MAIN_PERIODICITY_US."

#endif

#endif /* (STD_ON == I2C_ERROR_DETECTION_API) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the states of an I2C request. */
typedef enum
{
   /** \brief  The channel has triggered a start condition. */
   I2C_REQUEST_STATE_START,

   /** \brief  The channel has finished generating the start condition and is currently sending the slave address and
    *          request bit (read or write request). */
   I2C_REQUEST_STATE_ADDRESS,

   /** \brief  The channel has finished sending the slave address and is currently processing data bytes (reading or
    *          writing data bytes). */
   I2C_REQUEST_STATE_DATA,

   /** \brief  The channel has finished processing the data bytes and has triggered a stop condition. */
   I2C_REQUEST_STATE_STOP,

   /** \brief  The channel is ready for a new request. */
   I2C_REQUEST_STATE_PENDING,

} I2c_RequestStateType;

/** \brief  Defines the I2C request type. */
typedef enum
{
   /** \brief  The channel only sends the control byte, without expecting an acknowledge. */
   I2C_REQUEST_WAKEUP,

   /** \brief  The channel sends data bytes to the slave. */
   I2C_REQUEST_WRITE,

   /** \brief  The channel requests reading data bytes from the slave. */
   I2C_REQUEST_READ,

} I2c_RequestType;

typedef struct
{
#if (STD_ON == I2C_ERROR_DETECTION_API)
   /** \brief  Tracks the protocol error timeout. */
   uint16 us_ErrorCounterProtocol;

   /** \brief  Tracks the bus off error timeout. Gets incremented with each successive protocol error. */
   uint16 us_ErrorCounterBusOff;
#endif

   /** \brief  Current state from the I2C state machine. */
   I2c_RequestStateType t_RequestState;

   /** \brief  Status to be reported via the get status function. */
   I2c_StatusType t_Status;

   /** \brief  Type, read or write, of the last received request. */
   I2c_RequestType t_Request;

   /** \brief  Address of the buffer in which to store the received data or from which to write the data bytes. */
   uint8* puc_RequestBuffer;

   /** \brief  How many data bytes to be processed in the request. */
   uint8 uc_RequestLength;

   /** \brief  Index of the current processed data byte. */
   uint8 uc_RequestIndex;

   /** \brief  Slave address to read from or to write to. */
   uint8 uc_SlaveAddress;
} I2c_ChannelControlType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Address to the selected PB configuration. Loaded in the initialization function. */
static I2c_ChannelConfigType* I2c_pt_ChannelsConfig;

/** \brief  Control information for all the channels. */
static I2c_ChannelControlType I2c_at_ChannelsControl[I2C_NUMBER_OF_CHANNELS];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == I2C_ERROR_DETECTION_API)

static void I2c_v_ResetChannel(I2c_ChannelType t_ChannelId);

static void I2c_v_SetupRequest(I2c_ChannelConfigType* pt_ChannelConfig, I2c_ChannelControlType* pt_ChannelControl,
   uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength);

#endif

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (STD_ON == I2C_ERROR_DETECTION_API)
/**
 * \brief      Resets the specified channel's I2C peripheral, reinitializing all the control registers, and sets up the
 *             error status.
 * \param      t_ChannelId: Channel to reset.
 * \return     -
 */
static void I2c_v_ResetChannel(I2c_ChannelType t_ChannelId)
{
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   /* Store the address of the specified channel configuration to avoid further indexing. */
   pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

   /* Store the address of the specified channel control structure to avoid further indexing. */
   pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

   /* Reset channel through software reset. */
   pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_SWRST;
   pt_ChannelConfig->pt_HardwareUnit->CR1 &= ~I2C_CR1_SWRST;

   /* Reinitialize channel control registers. */
   RegInit_gv_Masked32Bits(&pt_ChannelConfig->t_InitConfig);

   /* Enable channel. */
   pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_PE;

   pt_ChannelControl->us_ErrorCounterProtocol = 0U;

   if (pt_ChannelControl->us_ErrorCounterBusOff < I2C_ERROR_TIMEOUT_BUS_OFF_COUNT)
   {
      pt_ChannelControl->t_Status = I2C_STATUS_ERROR_PROTOCOL;
      pt_ChannelControl->us_ErrorCounterBusOff++;
   }
   else
   {
      pt_ChannelControl->t_Status = I2C_STATUS_ERROR_BUS_OFF;
   }
}
#endif

/**
 * \brief      Prepares the state machine and triggers a start condition for a channel.
 * \param      pt_ChannelConfig: All the initialization and control registers for a channel.
 * \param      pt_ChannelControl: All the state machine control data for a channel.
 * \param      uc_SlaveAddr: I2C slave address for which the request is meant.
 * \param      puc_DataBuffer: Data buffer to be used in the request.
 * \param      uc_DataLength: Length of the request.
 * \return     -
 */
static void I2c_v_SetupRequest(I2c_ChannelConfigType* pt_ChannelConfig, I2c_ChannelControlType* pt_ChannelControl,
   uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength)
{
   /* Trigger a start condition. */
   pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_START;

   if (pt_ChannelControl->t_Status <= I2C_STATUS_ERROR_PROTOCOL)
   {
      /* The channel is running normally. Set busy status. */
      pt_ChannelControl->t_Status = I2C_STATUS_BUSY;
   }
   else
   {
      /* A bus off error state is present. The status can be updated only on the first successfully processed request. */
   }

   /* Save request information and enter start state. */
#if (STD_ON == I2C_ERROR_DETECTION_API)
   pt_ChannelControl->us_ErrorCounterProtocol = 0U;
#endif
   pt_ChannelControl->puc_RequestBuffer = puc_DataBuffer;
   pt_ChannelControl->uc_RequestLength = uc_DataLength;
   pt_ChannelControl->uc_RequestIndex = 0U;
   pt_ChannelControl->uc_SlaveAddress = uc_SlaveAddr;
   pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_START;
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief     Sets up the I2C hardware units and loads the channel configurations for runtime use.
 * \param     pt_Config: Pointer to the post-build configuration data to be loaded.
 * \return    -
 */
void I2c_gv_Init(const I2c_ConfigType* pt_Config)
{
   I2c_ChannelType t_ChannelId = 0U;
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   I2c_pt_ChannelsConfig = (I2c_ChannelConfigType*) *pt_Config;

   for (t_ChannelId = 0; t_ChannelId < I2C_NUMBER_OF_CHANNELS; t_ChannelId++)
   {
      /* Store the address of the specified channel configuration to avoid further indexing. */
      pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

      /* Store the address of the specified channel control structure to avoid further indexing. */
      pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

      /* Initialize channel control registers. */
      RegInit_gv_Masked32Bits(&pt_ChannelConfig->t_InitConfig);

      /* Set default channel control values. */
#if (STD_ON == I2C_ERROR_DETECTION_API)
      pt_ChannelControl->us_ErrorCounterProtocol = 0U;
      pt_ChannelControl->us_ErrorCounterBusOff = 0U;
#endif
      pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_PENDING;
      pt_ChannelControl->t_Status = I2C_STATUS_IDLE;
      pt_ChannelControl->t_Request = I2C_REQUEST_WAKEUP;
      pt_ChannelControl->puc_RequestBuffer = NULL_PTR;
      pt_ChannelControl->uc_RequestLength = 0U;
      pt_ChannelControl->uc_RequestIndex = 0U;
      pt_ChannelControl->uc_SlaveAddress = 0U;

      /* Enable channel. */
      pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_PE;
   }
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Implements the I2C request state machine based on the current request type using a polling mechanism.
 * \param      -
 * \return     -
 */
void I2c_gv_Main(void)
{
   I2c_ChannelType t_ChannelId;
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   for (t_ChannelId = 0; t_ChannelId < I2C_NUMBER_OF_CHANNELS; t_ChannelId++)
   {
      /* Store the address of the specified channel configuration to avoid further indexing. */
      pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

      /* Store the address of the specified channel control structure to avoid further indexing. */
      pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

      switch (pt_ChannelControl->t_RequestState)
      {

#if (STD_ON == I2C_ERROR_DETECTION_API)
         case I2C_REQUEST_STATE_PENDING:
         {
            /* There is no ongoing request. Check if bus is busy. */
            if ((pt_ChannelConfig->pt_HardwareUnit->SR2 & I2C_SR2_BUSY) != 0U)
            {
               if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
               {
                  /* Protocol timeout error occurred. Reset channel. */
                  I2c_v_ResetChannel(t_ChannelId);
               }
               else
               {
                  /* SDA or SCL are low but no request is ongoing. Keep track of timeout counter. */
                  pt_ChannelControl->us_ErrorCounterProtocol++;
               }
            }
            else
            {
               /* Everything is okay. Reset protocol counter. */
               pt_ChannelControl->us_ErrorCounterProtocol = 0U;
            }
            break;
         }
#endif

         case I2C_REQUEST_STATE_START:
         {
            if ((pt_ChannelConfig->pt_HardwareUnit->SR1 & I2C_SR1_SB) != 0U)
            {
               /* Start bit has been set. Prepare the control byte. */
               switch (pt_ChannelControl->t_Request)
               {
                  case (I2C_REQUEST_WRITE):
                  case (I2C_REQUEST_WAKEUP):
                  {
                     /* Set slave address (7 MSBs) and clear request type bit (LSB = 0) for signaling write request. */
                     pt_ChannelConfig->pt_HardwareUnit->DR = (pt_ChannelControl->uc_SlaveAddress << 1U);
                     break;
                  }
                  case (I2C_REQUEST_READ):
                  {
                     /* Set slave address (7 MSBs) and set request type bit (LSB = 1) for signaling read request. */
                     pt_ChannelConfig->pt_HardwareUnit->DR = ((pt_ChannelControl->uc_SlaveAddress << 1U) | 0x01U);
                     break;
                  }
                  default:
                  {
                     break;
                  }
               }

#if (STD_ON == I2C_ERROR_DETECTION_API)
               pt_ChannelControl->us_ErrorCounterProtocol = 0U;
#endif
               /* Enter addressing state. */
               pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_ADDRESS;
            }
#if (STD_ON == I2C_ERROR_DETECTION_API)
            else if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
            {
               /* Protocol timeout error occurred. Reset channel. */
               I2c_v_ResetChannel(t_ChannelId);
            }
            else
            {
               /* Start bit not set yet. Keep track of timeout counter. */
               pt_ChannelControl->us_ErrorCounterProtocol++;
            }
#else
            else
            {
               /* Nothing to do. Start condition not yet done. */
            }
#endif
            break;
         }

         case I2C_REQUEST_STATE_ADDRESS:
         {
            if ((I2C_REQUEST_WAKEUP == pt_ChannelControl->t_Request)
               && ((pt_ChannelConfig->pt_HardwareUnit->SR1 & I2C_SR1_AF) != 0U))
            {
               /* Control byte has been sent without an acknowledge. Trigger stop condition and enter stopping state.*/
               pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_STOP;
               pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_STOP;
            }
            else if ((pt_ChannelConfig->pt_HardwareUnit->SR1 & I2C_SR1_ADDR) != 0U)
            {
               /* Address sent bit has been set. SR2 register for clearing the ADDR bit. */
               (void) pt_ChannelConfig->pt_HardwareUnit->SR2;

               switch (pt_ChannelControl->t_Request)
               {
                  case (I2C_REQUEST_WRITE):
                  {
                     /* Prepare the first data byte to be sent. */
                     pt_ChannelConfig->pt_HardwareUnit->DR = pt_ChannelControl->puc_RequestBuffer[0];

                     /* Update request data index, and enter data state. */
                     pt_ChannelControl->uc_RequestIndex++;

                     break;
                  }
                  case (I2C_REQUEST_READ):
                  {
                     /* Enable acknowledge of received bytes. */
                     pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_ACK;
                     break;
                  }
                  default:
                  {
                     break;
                  }
               }

#if (STD_ON == I2C_ERROR_DETECTION_API)
               pt_ChannelControl->us_ErrorCounterProtocol = 0U;
#endif
               pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_DATA;
            }
#if (STD_ON == I2C_ERROR_DETECTION_API)
            else if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
            {
               /* Protocol timeout error occurred. Reset channel. */
               I2c_v_ResetChannel(t_ChannelId);
            }
            else
            {
               /* Address sent bit not set yet. Keep track of timeout counter. */
               pt_ChannelControl->us_ErrorCounterProtocol++;
            }
#else
            else
            {
               /* Nothing to do. Address has not been sent yet. */
            }
#endif
            break;
         }

         case I2C_REQUEST_STATE_DATA:
         {
            switch (pt_ChannelControl->t_Request)
            {
               case (I2C_REQUEST_WRITE):
               {
                  if ((pt_ChannelConfig->pt_HardwareUnit->SR1 & (I2C_SR1_TXE)) != 0U)
                  {
                     /* Successful transmission of a data byte. */
                     if (pt_ChannelControl->uc_RequestIndex < pt_ChannelControl->uc_RequestLength)
                     {
                        /* Not all the bytes have been sent yet. Prepare the next byte. */
                        pt_ChannelConfig->pt_HardwareUnit->DR = pt_ChannelControl->puc_RequestBuffer[pt_ChannelControl
                           ->uc_RequestIndex];
                        pt_ChannelControl->uc_RequestIndex++;
                     }
                     else
                     {
                        /* Data write request finished. Trigger stop condition and enter stopping state.*/
                        pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_STOP;
                        pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_STOP;
                     }
#if (STD_ON == I2C_ERROR_DETECTION_API)
                     pt_ChannelControl->us_ErrorCounterProtocol = 0U;
#endif
                  }
#if (STD_ON == I2C_ERROR_DETECTION_API)
                  else if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
                  {
                     /* Protocol timeout error occurred. Reset channel. */
                     I2c_v_ResetChannel(t_ChannelId);
                  }
                  else
                  {
                     /* Data sent bits not set yet. Keep track of timeout counter. */
                     pt_ChannelControl->us_ErrorCounterProtocol++;
                  }
#else
                  else
                  {
                     /* Nothing to do. Current data byte has not been sent yet. */
                  }
#endif
                  break;
               }

               case (I2C_REQUEST_READ):
               {
                  if ((pt_ChannelConfig->pt_HardwareUnit->SR1 & (I2C_SR1_RXNE)) != 0U)
                  {
                     /* Successful reception of a data byte. */
                     if (pt_ChannelControl->uc_RequestIndex < pt_ChannelControl->uc_RequestLength)
                     {
                        /* Read the received byte. */
                        pt_ChannelControl->puc_RequestBuffer[pt_ChannelControl->uc_RequestIndex] = pt_ChannelConfig
                           ->pt_HardwareUnit->DR;
                        pt_ChannelControl->uc_RequestIndex++;
                     }
                     else
                     {
                        /* Unreachable branch. */
                     }

                     if (pt_ChannelControl->uc_RequestIndex == (pt_ChannelControl->uc_RequestLength - 1U))
                     {
                        /* One byte is left to be read. It shall not be acknowledged. */
                        pt_ChannelConfig->pt_HardwareUnit->CR1 &= ~I2C_CR1_ACK;
                     }
                     else if (pt_ChannelControl->uc_RequestIndex == (pt_ChannelControl->uc_RequestLength))
                     {
                        /* Last requested byte has just been read. Trigger stop condition and enter stopping state. */
                        pt_ChannelConfig->pt_HardwareUnit->CR1 |= I2C_CR1_STOP;
                        pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_STOP;
                     }
                     else
                     {
                        /* Nothing to do. More request bytes are pending. */
                     }

#if (STD_ON == I2C_ERROR_DETECTION_API)
                     pt_ChannelControl->us_ErrorCounterProtocol = 0U;
#endif
                  }
#if (STD_ON == I2C_ERROR_DETECTION_API)
                  else if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
                  {
                     /* Protocol timeout error occurred. Reset channel. */
                     I2c_v_ResetChannel(t_ChannelId);
                  }
                  else
                  {
                     /* Data sent bits not set yet. Keep track of timeout counter. */
                     pt_ChannelControl->us_ErrorCounterProtocol++;
                  }
#else
                  else
                  {
                     /* Nothing to do. Current data byte has not been sent yet. */
                  }
#endif
                  break;
               }

               default:
               {
                  break;
               }
            }
            break;
         }

         case I2C_REQUEST_STATE_STOP:
         {
            if ((pt_ChannelConfig->pt_HardwareUnit->CR1 & I2C_CR1_STOP) == 0U)
            {
#if (STD_ON == I2C_ERROR_DETECTION_API)
               pt_ChannelControl->us_ErrorCounterProtocol = 0U;
               pt_ChannelControl->us_ErrorCounterBusOff = 0U;
#endif
               /* Request finished successfully. */
               pt_ChannelControl->t_RequestState = I2C_REQUEST_STATE_PENDING;
               pt_ChannelControl->t_Status = I2C_STATUS_COMPLETED;
            }
#if (STD_ON == I2C_ERROR_DETECTION_API)
            else if (pt_ChannelControl->us_ErrorCounterProtocol >= I2C_ERROR_TIMEOUT_PROTOCOL_COUNT)
            {
               /* Protocol timeout error occurred. Reset channel. */
               I2c_v_ResetChannel(t_ChannelId);
            }
            else
            {
               /* Stop bit has not been cleared by hardware yet. Keep track of timeout counter. */
               pt_ChannelControl->us_ErrorCounterProtocol++;
            }
#else
            else
            {
               /* Nothing to do. Stop condition not yet done. */
            }
#endif
            break;
         }

         default:
         {
            break;
         }
      }
   }
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Returns the status of the requested I2C channel so that the user knows when and how the requests
 *             finished (successfully or not). Clears the completed status the first time it is read so that the user
 *             knows when the data has been processed.
 * \param      t_ChannelId: Channel for which to read the status.
 * \return     Status of the channel.
 */
I2c_StatusType I2c_gt_GetStatus(I2c_ChannelType t_ChannelId)
{
   I2c_StatusType t_Return;
   if (I2C_STATUS_COMPLETED == I2c_at_ChannelsControl[t_ChannelId].t_Status)
   {
      t_Return = I2C_STATUS_COMPLETED;
      I2c_at_ChannelsControl[t_ChannelId].t_Status = I2C_STATUS_IDLE;
   }
   else
   {
      t_Return = I2c_at_ChannelsControl[t_ChannelId].t_Status;
   }
   return t_Return;
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Triggers a wakeup request for the specified I2C channel and slave address, which only sends out the
 *             control byte without expecting the acknowledge.
 * \param      t_ChannelId: Channel for which to process the request.
 * \param      uc_SlaveAddr: Slave address to write to.
 * \return     -
 */
void I2c_gv_Wakeup(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr)
{
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   /* Store the address of the specified channel configuration to avoid further indexing. */
   pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

   /* Store the address of the specified channel control structure to avoid further indexing. */
   pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

   I2c_v_SetupRequest(pt_ChannelConfig, pt_ChannelControl, uc_SlaveAddr, NULL_PTR, 0U);

   pt_ChannelControl->t_Request = I2C_REQUEST_WAKEUP;
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Triggers a write request for the specified I2C channel, the specified slave address, using the specified
 *             write data buffer and specified data length. Starts the I2C state machine.
 * \param      t_ChannelId: Channel for which to process the request.
 * \param      uc_SlaveAddr: Slave address to write to.
 * \param      puc_DataBuffer: Starting address of the data to be transmitted.
 * \param      uc_DataLength: How many data bytes to be written.
 * \return     -
 */
void I2c_gv_Write(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength)
{
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   /* Store the address of the specified channel configuration to avoid further indexing. */
   pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

   /* Store the address of the specified channel control structure to avoid further indexing. */
   pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

   I2c_v_SetupRequest(pt_ChannelConfig, pt_ChannelControl, uc_SlaveAddr, puc_DataBuffer, uc_DataLength);

   pt_ChannelControl->t_Request = I2C_REQUEST_WRITE;
}

/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Triggers a read request from the specified I2C channel, the specified slave address, using the specified
 *             read data buffer and specified data length. Starts the I2C state machine.
 * \param      t_ChannelId: Channel for which to process the request.
 * \param      uc_SlaveAddr: Slave address to read from.
 * \param      puc_DataBuffer: Starting address where to store the received data.
 * \param      uc_DataLength: How many data bytes to be read.
 * \return     -
 */
void I2c_gv_Read(I2c_ChannelType t_ChannelId, uint8 uc_SlaveAddr, uint8 * puc_DataBuffer, uint8 uc_DataLength)
{
   I2c_ChannelConfigType* pt_ChannelConfig;
   I2c_ChannelControlType* pt_ChannelControl;

   /* Store the address of the specified channel configuration to avoid further indexing. */
   pt_ChannelConfig = &I2c_pt_ChannelsConfig[t_ChannelId];

   /* Store the address of the specified channel control structure to avoid further indexing. */
   pt_ChannelControl = &I2c_at_ChannelsControl[t_ChannelId];

   I2c_v_SetupRequest(pt_ChannelConfig, pt_ChannelControl, uc_SlaveAddr, puc_DataBuffer, uc_DataLength);

   pt_ChannelControl->t_Request = I2C_REQUEST_READ;
}

