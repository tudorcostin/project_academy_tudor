/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Port.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the necessary functions for rendering data on a 7 segment display.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Display.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Switches off all the segments of the display.
 * \param      -
 * \return     -
 */
void Display_Erase()
{
   /* Switch off all the segments of the display. */
   Dio_WritePort(DISPLAY, 0xFFFF);
}

/**
 * \brief      Renders the passed character onto the 7 segment display.
 * \param      Character - Valid values:
 *             Digits 1-7
 *             DISPLAY_CHAR_P
 *             DISPLAY_CHAR_N
 *             DISPLAY_CHAR_R
 *             DISPLAY_CHAR_D
 * \return     -
 */
void Display_Write(uint8 Character)
{
   switch (Character)
   {
      /* Digit '1'. */
      case (1):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         break;
      }
      /* Digit '2'. */
      case (2):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_D, STD_LOW);
         break;
      }
      /* Digit '3'. */
      case (3):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_D, STD_LOW);
         break;
      }
      /* Digit '4'. */
      case (4):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_F, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         break;
      }
      /* Digit '5'. */
      case (5):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_F, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_D, STD_LOW);
         break;
      }
      /* Digit '6'. */
      case (6):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_F, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_D, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         break;
      }
      /* Digit '7'. */
      case (7):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         break;
      }
      /* Letter 'P'. */
      case (DISPLAY_CHAR_P):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_A, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_F, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         break;
      }
      /* Letter 'r'. */
      case (DISPLAY_CHAR_R):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         break;
      }
      /* Letter 'n'. */
      case (DISPLAY_CHAR_N):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         break;
      }
      /* Letter 'd'. */
      case (DISPLAY_CHAR_D):
      {
         Display_Erase();
         Dio_WriteChannel(DISPLAY_SEGMENT_G, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_E, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_D, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_C, STD_LOW);
         Dio_WriteChannel(DISPLAY_SEGMENT_B, STD_LOW);
         break;
      }
      default:
      {
         /* The parameter is not a supported character. */
         break;
      }
   }

}

