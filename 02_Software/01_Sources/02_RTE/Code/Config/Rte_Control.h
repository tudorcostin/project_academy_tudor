/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Control.h
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the RTE read and write operations that are performed by the Control software
 *                component.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef RTE_CONTROL_H
#define RTE_CONTROL_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#define Rte_Write_MotorOut_MotorFan(x) (Rte_MotorIn_MotorFan_Buf = (*x))

#define Rte_IsUpdated_InTempHumi_Temperature() (Rte_TempHumi_InTempHumi_Temperature_UpdateFlag)
#define Rte_Read_InTempHumi_Temperature(x) \
   do \
   { \
      (*x) = Rte_TempHumi_Temperature_Buf; \
      Rte_TempHumi_InTempHumi_Temperature_UpdateFlag = FALSE; \
   } while(0)
#define Rte_IsUpdated_InTempHumi_Humidity() (Rte_TempHumi_InTempHumi_Humidity_UpdateFlag)
#define Rte_Read_InTempHumi_Humidity(x) \
   do \
   { \
      (*x) = Rte_TempHumi_Humidity_Buf; \
      Rte_TempHumi_InTempHumi_Humidity_UpdateFlag = FALSE; \
   } while(0)

#define Rte_IsUpdated_InLuminosity_Luminosity() (Rte_Luminosity_InLuminosity_Luminosity_UpdateFlag)
#define Rte_Read_InLuminosity_Luminosity(x) \
   do \
   { \
      (*x) = Rte_Luminosity_Luminosity_Buf; \
      Rte_Luminosity_InLuminosity_Luminosity_UpdateFlag = FALSE; \
   } while(0)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* RTE_CONTROL_H */