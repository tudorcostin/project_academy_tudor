/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     RTE Generator
 *    \brief      Generated file - shall not be manually edited. 
 *                Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Rte_Control.h"
#include "Rte.h"
#include "Os.h"
#include "Pwm.h"
#include "Adc.h"
#include "Dio.h"
#include "Servo.h"
#include "Pot.h"
#include "Display.h"
#include "Button.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Enumeration type containing the possible states that the gear can be into. */
typedef enum
{
   /* Current gear mode is PARK. */
   GEAR_MODE_PARK,
   /* Current gear mode is NEUTRAL. */
   GEAR_MODE_NEUTRAL,
   /* Current gear mode is REVERSE. */
   GEAR_MODE_REVERSE,
   /* Current gear mode is DRIVE. */
   GEAR_MODE_DRIVE,
} GearType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Stores the current gear Mode*/
static GearType CurrentGear = GEAR_MODE_PARK;
/** \brief Flag used to store the Park button intermediate states. */
static boolean FlagParkButton = FALSE;
/** \brief Flag used to store the Neutral button intermediate states. */
static boolean FlagNeutralButton = FALSE;
/** \brief Flag used to store the Reverse button intermediate states. */
static boolean FlagReverseButton = FALSE;
/** \brief Flag used to store the Drive button intermediate states. */
static boolean FlagDriveButton = FALSE;
/** \brief Angle of the potentiometer. */
static uint16 PotSpeed = 0;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
   /* Clear the display. */
   Display_Erase();
   /* Initialize the potentiometer. */
   Pot_Init();
}

/**
 * \brief      Application specific OS_FAST_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{

   switch (CurrentGear)
   {
      case (GEAR_MODE_PARK):
      {
         /* Display the current drive mode. */
         Display_Write(DISPLAY_CHAR_P);
         /* Reset the servo motor. */
         Servo_SetAngle(0);
         /* We check to see if the Neutral switch button was pressed. */
         if (Buttons_Press(BUTTON_NEUTRAL, &FlagNeutralButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagNeutralButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Neutral gear mode. */
            CurrentGear = GEAR_MODE_NEUTRAL;
         }
         /* We check to see if the Reverse switch button was pressed. */
         if (Buttons_Press(BUTTON_REVERSE, &FlagReverseButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagReverseButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Reverse gear mode. */
            CurrentGear = GEAR_MODE_REVERSE;
         }
         /* We check to see if the Drive switch button was pressed. */
         if (Buttons_Press(BUTTON_DRIVE, &FlagDriveButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagDriveButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Drive gear mode. */
            CurrentGear = GEAR_MODE_DRIVE;
         }
         break;
      }
      case (GEAR_MODE_NEUTRAL):
      {
         /* Display the current drive mode. */
         Display_Write(DISPLAY_CHAR_N);
         /* Reset the servo motor. */
         Servo_SetAngle(0);
         /* We check to see if the Park switch button was pressed. */
         if (Buttons_Press(BUTTON_PARK, &FlagParkButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagParkButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Park gear mode. */
            CurrentGear = GEAR_MODE_PARK;
         }
         /* We check to see if the Reverse switch button was pressed. */
         if (Buttons_Press(BUTTON_REVERSE, &FlagReverseButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagReverseButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Reverse gear mode. */
            CurrentGear = GEAR_MODE_REVERSE;
         }
         /* We check to see if the Drive switch button was pressed. */
         if (Buttons_Press(BUTTON_DRIVE, &FlagDriveButton) == TRUE)
         {
            /* Reset the flag used by the button function. */
            FlagDriveButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Drive gear mode. */
            CurrentGear = GEAR_MODE_DRIVE;
         }
         break;
      }
      case (GEAR_MODE_REVERSE):
      {
         /* Display the current drive mode. */
         Display_Write(DISPLAY_CHAR_R);
         /* Get the angle of the potentiometer. */
         PotSpeed = Pot_GetSpeed();
         /* We check to see if the Park switch button was pressed. */
         if ((Buttons_Press(BUTTON_PARK, &FlagParkButton) == TRUE) && (PotSpeed < 5))
         {
            /* Reset the flag used by the button function. */
            FlagParkButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Park gear mode. */
            CurrentGear = GEAR_MODE_PARK;
         }
         /* We check to see if the Reverse switch button was pressed. */
         if ((Buttons_Press(BUTTON_NEUTRAL, &FlagNeutralButton) == TRUE) && (PotSpeed < 5))
         {
            /* Reset the flag used by the button function. */
            FlagNeutralButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Neutral gear mode. */
            CurrentGear = GEAR_MODE_NEUTRAL;
         }
         /* Max speed in reverse is 40 km/h. */
         if (PotSpeed > 40)
         {
            PotSpeed = 40;
         }
         /* Set the angle on the servo. */
         Servo_SetAngle(PotSpeed);
         break;
      }
      case (GEAR_MODE_DRIVE):
      {
         /* Get the angle of the potentiometer. */
         PotSpeed = Pot_GetSpeed();
         if (PotSpeed < 5)
         {
            /* Display the current drive mode. */
            Display_Write(DISPLAY_CHAR_D);
         }
         else if ((PotSpeed >= 5) && (PotSpeed <= 140))
         {
            /* Display current gear. */
            Display_Write((PotSpeed / 20) + 1);
         }
         /* We check to see if the Park switch button was pressed. */
         if ((Buttons_Press(BUTTON_PARK, &FlagParkButton) == TRUE) && (PotSpeed < 5))
         {
            /* Reset the flag used by the button function. */
            FlagParkButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Park gear mode. */
            CurrentGear = GEAR_MODE_PARK;
         }
         /* We check to see if the Drive switch button was pressed. */
         if ((Buttons_Press(BUTTON_NEUTRAL, &FlagNeutralButton) == TRUE) && (PotSpeed < 5))
         {
            /* Reset the flag used by the button function. */
            FlagNeutralButton = FALSE;
            /* Clear the display. */
            Display_Erase();
            /* Switch to the Neutral gear mode. */
            CurrentGear = GEAR_MODE_NEUTRAL;
         }

         /* Set the angle on the servo. */
         Servo_SetAngle(PotSpeed);

         break;
      }
      default:
      {
         break;
      }
   }
}
/**
 * \brief      Application specific OS_5MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{
}

/**
 * \brief      Application specific OS_10MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_10MS_TASK)
{
}

/**
 * \brief      Application specific OS_20MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{
}

/**
 * \brief      Application specific OS_100MS_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_100MS_TASK)
{
}

/**
 * \brief      Application specific OS_BACKGROUND_TASK task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}

