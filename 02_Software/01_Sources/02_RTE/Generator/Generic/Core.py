import xlwings as xw
import shutil
from pathlib import Path
import tkinter as tk
import time

from Common import *
import Types
import Interfaces
import Modules
import Scheduling

root = tk.Tk()

def add_module(file_name):
	match = re.match(r'^[A-Z][a-zA-Z0-9]{1,19}$', file_name)
	if match != None:
		# copy template and create new file from it with the name from parameter
		dirname = os.path.abspath(os.path.dirname(__file__))
		if not os.path.exists(dirname + '/../Config/Modules'):
			os.makedirs(dirname + '/../Config/Modules')
		if not os.path.isfile(dirname + '/../Config/Modules/' + file_name + '.xlsm'):
			shutil.copy(dirname + '/TemplateModule.xlsm', dirname + '/../Config/Modules/' + file_name + '.xlsm')
			# write to new file
			wb = xw.Book(dirname + '/../Config/Modules/' + file_name + '.xlsm')
			wb.sheets[0].name = file_name
			wb.save()

			# write the file to Main
			wb = xw.Book(dirname + '/../Config/Main.xlsm')
			wb.sheets["Main"].range("B" + str(int(wb.sheets["Main"].range('A1').value) + 1)).value = file_name
			wb.save()
		else:
			messagebox.showerror("Error", "Duplicate name: \"" + file_name + "\"!")
			root.destroy()
	else:
		messagebox.showerror("Error", "The module name has invalid format.")
		root.destroy()

def open_module(button_name):
	button_name = button_name.replace('Open_', '')
	dirname = os.path.abspath(os.path.dirname(__file__))
	
	wb = xw.Book(dirname + '/../Config/Modules/' + button_name + '.xlsm').activate(steal_focus = True)

def delete_module(button_name):
	button_name = button_name.replace('Delete_', '')
	dirname = os.path.abspath(os.path.dirname(__file__))
	
	os.remove(dirname + '/../Config/Modules/' + button_name + '.xlsm')

def open_core(button_name):
	button_name = button_name.replace('Open_', '')
	dirname = os.path.abspath(os.path.dirname(__file__))

	xw.Book(dirname + '/' + button_name + '.xlsm').activate(steal_focus = True)

#RETURN FUNCTIONS
def main():
	dirname = os.path.abspath(os.path.dirname(__file__))
	xw.books.active.save()
	xw.books.active.close()
	xw.Book(dirname+'/../Config/Main.xlsm').activate(steal_focus = True)

def interfaces():
	dirname = os.path.abspath(os.path.dirname(__file__))
	xw.books.active.save()
	file_name = Path(dirname + "/../Config/Interfaces.xlsm")
	if not file_name.is_file():
		shutil.copy(dirname + '/TemplateInterfaces.xlsm', dirname + '/../Config/Interfaces.xlsm')
	xw.Book(dirname + '/../Config/Interfaces.xlsm').activate(steal_focus = True)

def types():
	dirname = os.path.abspath(os.path.dirname(__file__))
	xw.books.active.save()
	file_name = Path(dirname + "/../Config/Types.xlsm")
	if not file_name.is_file():
		shutil.copy(dirname + '/TemplateTypes.xlsm', dirname + '/../Config/Types.xlsm')
	xw.Book(dirname+'/../Config/Types.xlsm').activate(steal_focus = True)

def scheduling():
	dirname = os.path.abspath(os.path.dirname(__file__))
	xw.books.active.save()
	file_name = Path(dirname + "/../Config/Scheduling.xlsm")
	if not file_name.is_file():
		shutil.copy(dirname + '/TemplateScheduling.xlsm', dirname + '/../Config/Scheduling.xlsm')
	xw.Book(dirname+'/../Config/Scheduling.xlsm').activate(steal_focus = True)

#START GENERATOR
cluster = []
sdtypes = []
log = Log()

class Cluster:
	def __init__(self, name):
		self.name = name
		self.component = []
	def add_component(self, component):
		self.component.append(component)

class Core:
	def __init__(self, cluster_name):
		self.cluster_name = cluster_name
		self.interface_type = []
		self.directions = []
		self.functions = []
	def add_function(self, direction, function):
		self.directions.append(direction)
		self.functions.append(function)

class SdType:
	def __init__(self, name):
		self.name = name
		self.type = "SdType"

def reader_core():
	wb_core = xw.Book(dirname + "/Core.xlsm")
	len_core = int(wb_core.sheets["StandardInterfaces"].range(TOTAL_LINES_POS).value)
	len_sdtype = int(wb_core.sheets["StandardDTypes"].range(TOTAL_LINES_POS).value)
	matrix_core = wb_core.sheets["StandardInterfaces"].range(CORE_CLUSTER_POS + str(START_CELL_POS), CORE_FUNC_N_POS + str(len_core + 1)).value
	array_sdtype = wb_core.sheets["StandardDTypes"].range(CORE_SDTYPE_N_POS + str(START_CELL_POS), CORE_SDTYPE_N_POS + str(len_sdtype + 1)).value
	len_core = len_core - MINUS_VAL
	len_sdtype = len_sdtype - MINUS_VAL

	for i in range(0, len_core):
		if matrix_core[i][0] != None:
			cluster.append(Cluster(matrix_core[i][0]))
		
		if matrix_core[i][1] != None:
			cluster[len(cluster) - 1].add_component(Core(matrix_core[i][1]))
		
		if matrix_core[i][2] == None or matrix_core[i][3] == None:
			messagebox.showerror("Error", "Invalid syntax in Core:StandardInterfaces at the line " + str(i + MINUS_VAL + 1) + ".")
			return 1
		
		if matrix_core[i][2] != 'R' and matrix_core[i][2] != 'P':
			messagebox.showerror("Error", "Invalid direction in Core:StandardInterfaces at the line " + str(i + MINUS_VAL + 1) + ".")
			return 1

		cluster[len(cluster) - 1].component[len(cluster[len(cluster)-1].component) - 1].add_function(matrix_core[i][2], matrix_core[i][3])
	
	for i in range(0, len_sdtype):
		if array_sdtype[i] == None:
			messagebox.showerror("Error", "Invalid syntax in Core:StandardDTypes at the line " + str(i + MINUS_VAL + 1) + ".")
			return 1
		
		sdtypes.append(SdType(array_sdtype[i]))
	
	return 0

def generate():
	if os.path.exists(dirname + '/../../Code/Config'):
		shutil.rmtree(dirname + '/../../Code/Config')
	if not os.path.exists(dirname + '/../../Code/Config'):
		os.makedirs(dirname + '/../../Code/Config')
	if reader_core() != 1:
		if Types.reader(sdtypes) != 1:
			if Interfaces.reader(Types.data) != 1:
				Types.check()
				if Modules.reader(Interfaces.data) != 1:
					Interfaces.check()
					if Scheduling.reader(Modules.data) != 1:
						Modules.check()
	
	log.errors = log.errors + Types.log.errors + Interfaces.log.errors + Modules.log.errors + Scheduling.log.errors
	log.warnings = log.warnings + Types.log.warnings + Interfaces.log.warnings + Modules.log.warnings + Scheduling.log.warnings
	
	if log.errors == "":
		Types.writer()
		Modules.writer(Interfaces.data, cluster)
		Scheduling.writer(Modules.data)
		messagebox.showinfo("Message","Success!")
		
	if log.errors != "":
		messagebox.showerror("Error", "One or more errors have been found! Check the data log for more info.")
	if log.warnings != "":
		messagebox.showwarning("Warning", "One or more warnings have been found! Check the data log for more info.")
	
	if log.errors != "" or log.warnings != "":
		if not os.path.exists(dirname + '/../Config/Log'):
			os.makedirs(dirname + '/../Config/Log')
		
		localtime = time.localtime(time.time())
		file = open(dirname + "/../Config/Log/Generate_" + str(localtime[0]) + "_" + str(localtime[1]) + "_" + str(localtime[2]) + "-" + str(localtime[3]) + "_" + str(localtime[4]) + "_" + str(localtime[5]) + ".log", "w+")
		file.write("Errors\n\n" + log.errors + "\nWarnings\n\n" + log.warnings)
		file.close()
	
	root.destroy()
	
#generate()