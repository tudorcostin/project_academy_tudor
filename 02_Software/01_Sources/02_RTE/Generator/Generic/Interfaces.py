import xlwings as xw
import os.path
from Common import *

class Struct_Init:
	def __init__(self):
		self.value = []
	def add_Value(self, value):
		self.value.append(value)

class Buffer:
	def __init__(self, name):
		self.name = name
		self.type = "Buffer"
		self.var_data = []
		self.dtype = []
		self.initval = []
		self.updateflag = []
		self.used_r = 0
		self.used_p = 0
	def add_Member(self, var_data, dtype, initval, updateflag):
		self.var_data.append(var_data)
		self.dtype.append(dtype)
		self.initval.append(initval)
		self.updateflag.append(updateflag)
	def check_duplicate(self, name):
		if name in self.var_data:
			return 1
		else:
			return 0

class IoHwAb:
	def __init__(self, name):
		self.name = name
		self.type = "IoHwAb"
		self.port_type = []
		self.var_data = []
		self.channel_name = []
	def add_Member(self, port_type, var_data, channel_name):
		self.port_type.append(port_type)
		self.var_data.append(var_data)
		self.channel_name.append(channel_name)
	def check_duplicate(self, name):
		if name in self.var_data:
			return 1
		else:
			return 0

class Interface:
	def __init__(self):
		self.buffer = []
		self.iohwab = []
	def add_Buffer(self, buffer):
		self.buffer.append(buffer)
	def add_IoHwAb(self, iohwab):
		self.iohwab.append(iohwab)
	def check_duplicate(self, name):
		len_buffer = len(self.buffer)
		for i in range(0, len_buffer):
			if name == self.buffer[i].name:
				return 1
		
		len_iohwab = len(self.iohwab)
		for i in range(0, len_iohwab):
			if name == self.iohwab[i].name:
				return 1
		
		return 0

data = Interface()
log = Log()

'''
def check_struct_init(struct):
	len_struct = len(struct.member_type)
	for i in range(0, len_struct):
		if struct.member_type[i].type == "Struct":
			check_struct_init(struct.member_type[i])
		elif struct.member_type[i].type == "Enum":
		
		elif struct.member_type[i].type == "TypeDef":
		
		elif struct.member_type[i].type == "SdType":
'''

def reader(types):
	wb_data = xw.Book(dirname + '/../Config/Interfaces.xlsm')
	len_buffer = int(wb_data.sheets["Buffer"].range(TOTAL_LINES_POS).value)
	len_iohwab = int(wb_data.sheets["IoHwAb"].range(TOTAL_LINES_POS).value)
	matrix_buffer = wb_data.sheets["Buffer"].range(BUFF_INTERF_POS + str(START_CELL_POS), BUFF_UPDATE_FLAG_POS + str(len_buffer + 1)).value
	matrix_iohwab = wb_data.sheets["IoHwAb"].range(IOHWAB_INTERF_POS + str(START_CELL_POS), IOHWAB_CH_N_POS + str(len_iohwab + 1)).value
	wb_data.close()
	len_buffer = len_buffer - MINUS_VAL
	len_iohwab = len_iohwab - MINUS_VAL
	
	found_struct = 0
	#BUFFER_READER
	for i in range(0, len_buffer):
		if matrix_buffer[i][0] != None:
			if check_name(matrix_buffer[i][0]) == 1:
				log.errors = log.errors + "Invalid interface name in Interfaces:Buffer at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_buffer[i][0] + ")\n"
			
			if data.check_duplicate(matrix_buffer[i][0]):
				log.errors = log.errors + "Interface:Buffer name \"" + matrix_buffer[i][0] + "\" is duplicate.\n"
			
			buffer = Buffer(matrix_buffer[i][0])
			data.add_Buffer(buffer)
		
		if matrix_buffer[i][1] != None:
			found_struct = 0
			struct_values = Struct_Init()
		
		if found_struct == 0:
			if matrix_buffer[i][1] == None or matrix_buffer[i][2] == None or matrix_buffer[i][3] == None:
				log.errors = log.errors + "Invalid syntax for \"" + data.buffer[len(data.buffer) - 1].name + "\" interface from Interfaces:Buffer at the line " + str(i + MINUS_VAL + 1) + "!\n"
				return 1
			
			if matrix_buffer[i][4] != None and matrix_buffer[i][4] != 'x':
				log.errors = log.errors + "Invalid flag symbol for \"" + data.buffer[len(data.buffer) - 1].name + "\" interface from Interfaces:Buffer at the line " + str(i + MINUS_VAL + 1) + "!\n"
				return 1
			
			if check_var(matrix_buffer[i][1]) == 1:
				log.errors = log.errors + "Invalid variable data prototype name in Interfaces:Buffer at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_buffer[i][1] + ")\n"
			
			if buffer.check_duplicate(matrix_buffer[i][1]) == 1:
				log.errors = log.errors + "Interface:Buffer:VariableDataPrototype name \"" + matrix_buffer[i][1] + "\" is duplicate.\n"
			
			#duplicate
			found_type = 0
			len_typedef = len(types.typedef)
			for j in range(0, len_typedef):
				if matrix_buffer[i][2] == types.typedef[j].name:
					found_type = 1
					types.typedef[j].used = 1
					if check_init(types.typedef[j].base_type.name, matrix_buffer[i][3]) == 1:
						log.errors = log.errors + matrix_buffer[i][3] + " is invalid init value for \"" + types.typedef[j].name + "\"!\n"
					
					buffer.add_Member(matrix_buffer[i][1], types.typedef[j], matrix_buffer[i][3], matrix_buffer[i][4])
			
			len_enum = len(types.enum)
			for j in range(0, len_enum):
				if matrix_buffer[i][2] == types.enum[j].name:
					found_type = 1
					types.enum[j].used = 1
					buffer.add_Member(matrix_buffer[i][1], types.enum[j], matrix_buffer[i][3], matrix_buffer[i][4])
			
			len_sdtypes = len(types.sdtype)
			for j in range(0, len_sdtypes):
				if matrix_buffer[i][2] == types.sdtype[j].name:
					found_type = 1
					if check_init(types.sdtype[j].name, matrix_buffer[i][3]) == 1:
						log.errors = log.errors + matrix_buffer[i][3] + " is invalid init value for \"" + types.sdtype[j].name + "\"!\n"
					
					buffer.add_Member(matrix_buffer[i][1], types.sdtype[j], matrix_buffer[i][3], matrix_buffer[i][4])
			#end_duplicate
			
			len_struct = len(types.struct)
			for j in range(0, len_struct):
				if matrix_buffer[i][2] == types.struct[j].name:
					found_struct = 1
					types.struct[j].used = 1
					struct_values.add_Value(matrix_buffer[i][3])					
					buffer.add_Member(matrix_buffer[i][1], types.struct[j], struct_values, matrix_buffer[i][4])
			
			if found_type == 0 and found_struct == 0:
				log.errors = log.errors + "Type \"" + matrix_buffer[i][2] + "\" from Interfaces:Buffer at the line " + str(i + MINUS_VAL + 1) + " was not found!\n"
				return 1
		
		else:
			struct_values.add_Value(matrix_buffer[i][3])

	'''
	#CHECK_STRUCT_INIT
	len_buffer = len(data.buffer)
	for i in range(0, len_buffer):
		len_dtype = len(data.buffer[i].dtype)
		for j in range(0, len_dtype):
			if data.buffer[i].dtype[j].type == "Struct":
				check_struct_init(data.buffer[i].dtype[j])
	'''
	
	#READER_IOHWAB
	for i in range(0, len_iohwab):
		if matrix_iohwab[i][0] != None:
			if check_name(matrix_iohwab[i][0]) == 1:
				log.errors = log.errors + "Invalid interface name in Interfaces:IoHwAb at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_iohwab[i][0] + ")\n"
			
			if data.check_duplicate(matrix_iohwab[i][0]) == 1:
				log.errors = log.errors + "Interface:IoHwAb name \"" + matrix_iohwab[i][0] + "\" is duplicate.\n"
			iohwab = IoHwAb(matrix_iohwab[i][0])
			data.add_IoHwAb(iohwab)
		
		if matrix_iohwab[i][1] == None or matrix_iohwab[i][2] == None or matrix_iohwab[i][3] == None:
			log.errors = log.errors + "Invalid syntax for \"" + data.iohwab[len(data.iohwab) - 1].name + "\" interface from Interfaces:IoHwAb at the line " + str(i + MINUS_VAL + 1) + "!\n"
			return 1
		
		if check_var(matrix_iohwab[i][2]) == 1:
			log.errors = log.errors + "Invalid variable data prototype name in Interfaces:IoHwAb at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_iohwab[i][2] + ")\n"
		
		if check_iohwabchannel(matrix_iohwab[i][3]) == 1:
			log.errors = log.errors + "Invalid channel name in Interfaces:IoHwAb at the line " + str(i + MINUS_VAL + 1) + ".(" + matrix_iohwab[i][3] + ")\n"
			
		if iohwab.check_duplicate(matrix_iohwab[i][2]) == 1:
			log.errors = log.errors + "Interface:IoHwAb:VariableDataPrototype name \"" + matrix_iohwab[i][2] + "\" is duplicate.\n"
		
		iohwab.add_Member(matrix_iohwab[i][1], matrix_iohwab[i][2], matrix_iohwab[i][3])
	
	return 0

def check():
	len_buffer = len(data.buffer)
	for i in range(0, len_buffer):
		if data.buffer[i].used_p == 0 and data.buffer[i].used_r == 0:
			log.errors = log.errors + "Interface \"" + data.buffer[i].name + "\" is not linked to any provider and require.\n"
		else:
			if data.buffer[i].used_p == 0:
				log.warnings = log.warnings + "Interface \"" + data.buffer[i].name + "\" is not linked to provider.\n"
			if data.buffer[i].used_r == 0:
				log.warnings = log.warnings + "Interface \"" + data.buffer[i].name + "\" is not linked to require.\n"
	
	return 0