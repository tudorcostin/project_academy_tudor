/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Port_PBcfg.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Sets up the Port configuration tables using the RegInit data types to enable the clocks, pin mode,
 *                direction, output type, slew rate, pull-up/pull-down.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Port.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/** \brief Define the number of initialized registers. */
#define PORT_NUMBER_OF_INIT_REGISTERS (50U)
/** \brief Define the digital input state register value. */
#define PORT_MODER_MODER_INPUT (0x00000000U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
/** \brief Stores the initialization configuration for all the port registers  */
static const RegInit_Masked32BitsSingleType Port_kat_Registers[PORT_NUMBER_OF_INIT_REGISTERS] =
   {
/*
* ------------------------------------------------------PORT C--------------------------------------------------------
* */
      /* Set the slew rate to high for Port C Pin 6,7,8,9,10,11,12. */
      { &GPIOC->OSPEEDR, ~(
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR10 |
      GPIO_OSPEEDER_OSPEEDR11 |
      GPIO_OSPEEDER_OSPEEDR12
      ), (
      GPIO_OSPEEDER_OSPEEDR6 |
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR10 |
      GPIO_OSPEEDER_OSPEEDR11 |
      GPIO_OSPEEDER_OSPEEDR12
      ) },
      /* Set the direction for PORT C Pin 6,7,8,9,10,11,12 to General purpose Output */
      { &GPIOC->MODER, ~(
      GPIO_MODER_MODER6_0 |
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER8_0 |
      GPIO_MODER_MODER9_0 |
      GPIO_MODER_MODER10_0 |
      GPIO_MODER_MODER11_0 |
      GPIO_MODER_MODER12_0
      ), (
      GPIO_MODER_MODER6_0 |
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER8_0 |
      GPIO_MODER_MODER9_0 |
      GPIO_MODER_MODER10_0 |
      GPIO_MODER_MODER11_0 |
      GPIO_MODER_MODER12_0
      ) },

      /* Set the output type for Port C Pin 6,7,8,9,10,11,12 to pull up. */
      { &GPIOC->PUPDR, ~(
      GPIO_PUPDR_PUPDR6 |
      GPIO_PUPDR_PUPDR7 |
      GPIO_PUPDR_PUPDR8 |
      GPIO_PUPDR_PUPDR9 |
      GPIO_PUPDR_PUPDR10 |
      GPIO_PUPDR_PUPDR11 |
      GPIO_PUPDR_PUPDR12
      ), (
      GPIO_PUPDR_PUPDR6_0 |
      GPIO_PUPDR_PUPDR7_0 |
      GPIO_PUPDR_PUPDR8_0 |
      GPIO_PUPDR_PUPDR9_0 |
      GPIO_PUPDR_PUPDR10_0 |
      GPIO_PUPDR_PUPDR11_0 |
      GPIO_PUPDR_PUPDR12_0
      ) },
/*
* ------------------------------------------------------PORT E--------------------------------------------------------
* */
      /* Set the slew rate to high for Port E Pin 7,8,9,10,11,12,13. */
      { &GPIOE->OSPEEDR, ~(
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR10 |
      GPIO_OSPEEDER_OSPEEDR11 |
      GPIO_OSPEEDER_OSPEEDR12 |
      GPIO_OSPEEDER_OSPEEDR13
      ), (
      GPIO_OSPEEDER_OSPEEDR7 |
      GPIO_OSPEEDER_OSPEEDR8 |
      GPIO_OSPEEDER_OSPEEDR9 |
      GPIO_OSPEEDER_OSPEEDR10 |
      GPIO_OSPEEDER_OSPEEDR11 |
      GPIO_OSPEEDER_OSPEEDR12 |
      GPIO_OSPEEDER_OSPEEDR13
      ) },
      /* Set the direction for PORT Port E Pin 7,8,9,10,11,12,13 to General purpose Output */
      { &GPIOE->MODER, ~(
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER8_0 |
      GPIO_MODER_MODER9_0 |
      GPIO_MODER_MODER10_0 |
      GPIO_MODER_MODER11_0 |
      GPIO_MODER_MODER12_0 |
      GPIO_MODER_MODER13_0
      ), (
      GPIO_MODER_MODER7_0 |
      GPIO_MODER_MODER8_0 |
      GPIO_MODER_MODER9_0 |
      GPIO_MODER_MODER10_0 |
      GPIO_MODER_MODER11_0 |
      GPIO_MODER_MODER12_0 |
      GPIO_MODER_MODER13_0
      ) },

      /* Set the output type for Port Port E Pin 7,8,9,10,11,12,13 to pull up. */
      { &GPIOE->PUPDR, ~(
      GPIO_PUPDR_PUPDR7 |
      GPIO_PUPDR_PUPDR8 |
      GPIO_PUPDR_PUPDR9 |
      GPIO_PUPDR_PUPDR10 |
      GPIO_PUPDR_PUPDR11 |
      GPIO_PUPDR_PUPDR12 |
      GPIO_PUPDR_PUPDR13
      ), (
      GPIO_PUPDR_PUPDR7_0 |
      GPIO_PUPDR_PUPDR8_0 |
      GPIO_PUPDR_PUPDR9_0 |
      GPIO_PUPDR_PUPDR10_0 |
      GPIO_PUPDR_PUPDR11_0 |
      GPIO_PUPDR_PUPDR12_0 |
      GPIO_PUPDR_PUPDR13_0
      ) },
/*
* ------------------------------------------------------PORT C--------------------------------------------------------
* */

/* Set port D pins 8,9,10,11 to general purpose input. */
      {
         (volatile uint32*) &GPIOD->MODER,

         (uint32) ~(
         GPIO_MODER_MODER8 |
         GPIO_MODER_MODER9 |
         GPIO_MODER_MODER10 |
         GPIO_MODER_MODER11
         ),

         (uint32) (0U),
      },
      /* Set port D pins 8,9,10,11 to very high speed. */
      {
         (volatile uint32*) (&GPIOD->OSPEEDR),

         (uint32) ~(
         GPIO_OSPEEDER_OSPEEDR8 |
         GPIO_OSPEEDER_OSPEEDR9 |
         GPIO_OSPEEDER_OSPEEDR10 |
         GPIO_OSPEEDER_OSPEEDR11),

         (uint32) (
         GPIO_OSPEEDER_OSPEEDR8 |
         GPIO_OSPEEDER_OSPEEDR9 |
         GPIO_OSPEEDER_OSPEEDR10 |
         GPIO_OSPEEDER_OSPEEDR11),
      },
      /* Set port D pins 8,9,10,11 to Pull-up. */
      {
         (volatile uint32*) (&GPIOD->PUPDR),

         (uint32) ~(
         GPIO_PUPDR_PUPDR8 |
         GPIO_PUPDR_PUPDR9 |
         GPIO_PUPDR_PUPDR10 |
         GPIO_PUPDR_PUPDR11
         ),

         (uint32) (
         GPIO_PUPDR_PUPDR8_1 |
         GPIO_PUPDR_PUPDR9_1 |
         GPIO_PUPDR_PUPDR10_1 |
         GPIO_PUPDR_PUPDR11_1
         )
      },



/*
* ------------------------------------------------------PORT C--------------------------------------------------------
* */
      /* Set the slew rate to high for Port A Pin 1. */
      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR1), (GPIO_OSPEEDER_OSPEEDR1) },
      /* Set the direction for PORT A Pin 1to Alternate Function Mode . */
      { &GPIOA->MODER, ~(GPIO_MODER_MODER1), (GPIO_MODER_MODER1_1) },
      /* Set Alternate Function 1 (TIM2_CH2) for Port A Pin 1. */
      { &GPIOA->AFRL, ~(GPIO_AFRL_AFRL1), (GPIO_AFRL_AFRL1_0) },

/*
* ------------------------------------------------------PORT C--------------------------------------------------------
* */
      /* Set the output type for Port A Pin 0 to as open drain. */
      { &GPIOA->PUPDR, ~(GPIO_PUPDR_PUPDR0), (GPIO_PUPDR_PUPDR0_0) },
      { &GPIOA->OTYPER, ~(GPIO_OTYPER_ODR_0), (0x00) },
      /* Set the output type for Port A Pin 0 to as open drain. */
      /* Set the slew rate to high for Port A Pin 0. */
      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR0), (GPIO_OSPEEDER_OSPEEDR0) },
      /* Set the direction for PORT A Pin 0 to General purpose Output. */
      { &GPIOA->MODER, ~(GPIO_MODER_MODER0_0), (GPIO_MODER_MODER0_0) },
//      /* Set the slew rate to high for Port A Pin 1. */
//      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR1), (GPIO_OSPEEDER_OSPEEDR1) },
//      /* Set the direction for PORT A Pin 1 to Analog. */
//      { &GPIOA->MODER, ~(GPIO_MODER_MODER1), (GPIO_MODER_MODER1) },
      /* Set the slew rate to high for Port A Pin 2. */
      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR2), (GPIO_OSPEEDER_OSPEEDR2) },
      /* Set the direction for PORT A Pin 2 to digital input. */
      { &GPIOA->MODER, ~(PORT_MODER_MODER_INPUT), (PORT_MODER_MODER_INPUT) },
      /* Set the slew rate to high for Port A Pin 4. */
      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR4), (GPIO_OSPEEDER_OSPEEDR4) },
      /* Set the direction for PORT A Pin 4 to Analog. */
      { &GPIOA->MODER, ~(GPIO_MODER_MODER4), (GPIO_MODER_MODER4) },
      /* Set the slew rate to high for Port A Pin 7. */
      { &GPIOA->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR7), (GPIO_OSPEEDER_OSPEEDR7) },
      /* Set the direction for PORT A Pin 7 to General purpose Output */
      { &GPIOA->MODER, ~(GPIO_MODER_MODER7_0), (GPIO_MODER_MODER7_0) },
      /* Set the slew rate to high for Port B Pin 3. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR3), (GPIO_OSPEEDER_OSPEEDR3) },
      /* Set the direction for PORT B Pin 3 to Alternate Function Mode . */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER3), (GPIO_MODER_MODER3_1) },
      /* Set Alternate Function 1 (TIM2_CH2) for Port B Pin 3. */
      { &GPIOB->AFRL, ~(GPIO_AFRL_AFRL3), (GPIO_AFRL_AFRL3_0) },
      /* Set the slew rate to high for Port B Pin 5. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR5), (GPIO_OSPEEDER_OSPEEDR5) },
      /* Set the direction for PORT B Pin 5 to Alternate Function Mode . */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER5), (GPIO_MODER_MODER5_1) },
      /* Set Alternate Function 2 (TIM3_CH2) for Port B Pin 5. */
      { &GPIOB->AFRL, ~(GPIO_AFRL_AFRL5), (GPIO_AFRL_AFRL5_1) },
      /* Set the slew rate to high for Port A Pin 9. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR6), (GPIO_OSPEEDER_OSPEEDR6) },
      /* Set the direction for Port B Pin 6 to Alternate Function Mode . */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER6), (GPIO_MODER_MODER6_1) },
      /* Set Alternate Function 7 (USART1_TX) for Port B pin 6. */
      { &GPIOB->AFRL, ~(GPIO_AFRL_AFRL6), (GPIO_AFRL_AFRL6_0 | GPIO_AFRL_AFRL6_1 | GPIO_AFRL_AFRL6_2) },
      /* Set the slew rate to high for Port B Pin 7. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR7), (GPIO_OSPEEDER_OSPEEDR7) },
      /* Set the direction for Port B Pin 7 to Alternate Function Mode . */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER7), (GPIO_MODER_MODER7_1) },
      /* Set Alternate Function 7 (USART1_RX) for Port B Pin 7. */
      { &GPIOB->AFRL, ~(GPIO_AFRL_AFRL7), (GPIO_AFRL_AFRL7_0 | GPIO_AFRL_AFRL7_1 | GPIO_AFRL_AFRL7_2) },

      /* Set the slew rate to high for Port B Pin 8. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR8), (GPIO_OSPEEDER_OSPEEDR8) },
      /* Set Alternate Function 4 (I2C1 SCL) for Port B Pin 8. */
      { &GPIOB->AFRH, ~(GPIO_AFRH_AFRH8), (GPIO_AFRH_AFRH8_2) },
      /* Set the direction for Port B Pin 8 to Alternate Function Mode. */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER8), (GPIO_MODER_MODER8_1) },
      /* Set the output type for Port B Pin 8 to as open drain. */
      { &GPIOB->OTYPER, ~(GPIO_OTYPER_ODR_8), (GPIO_OTYPER_ODR_8) },

      /* Set the slew rate to high for Port B Pin 9. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR9), (GPIO_OSPEEDER_OSPEEDR9) },
      /* Set Alternate Function 4 (I2C1 SDA) for Port B Pin 9. */
      { &GPIOB->AFRH, ~(GPIO_AFRH_AFRH9), (GPIO_AFRH_AFRH9_2) },
      /* Set the direction for Port B Pin 9 to Alternate Function Mode. */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER9), (GPIO_MODER_MODER9_1) },
      /* Set the output type for Port B Pin 9 to as open drain. */
      { &GPIOB->OTYPER, ~(GPIO_OTYPER_ODR_9), (GPIO_OTYPER_ODR_9) },

      /* Set the slew rate to high for Port B Pin 10. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR10), (GPIO_OSPEEDER_OSPEEDR10) },
      /* Set Alternate Function 4 (I2C2 SCL) for Port B Pin 10. */
      { &GPIOB->AFRH, ~(GPIO_AFRH_AFRH10), (GPIO_AFRH_AFRH10_2) },
      /* Set the direction for Port B Pin 10 to Alternate Function Mode. */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER10), (GPIO_MODER_MODER10_1) },
      /* Set the output type for Port B Pin 10 to as open drain. */
      { &GPIOB->OTYPER, ~(GPIO_OTYPER_ODR_10), (GPIO_OTYPER_ODR_10) },

      /* Set the slew rate to high for Port B Pin 11. */
      { &GPIOB->OSPEEDR, ~(GPIO_OSPEEDER_OSPEEDR11), (GPIO_OSPEEDER_OSPEEDR11) },
      /* Set Alternate Function 4 (I2C2 SDA) for Port B Pin 11. */
      { &GPIOB->AFRH, ~(GPIO_AFRH_AFRH11), (GPIO_AFRH_AFRH11_2) },
      /* Set the direction for Port B Pin 11 to Alternate Function Mode. */
      { &GPIOB->MODER, ~(GPIO_MODER_MODER11), (GPIO_MODER_MODER11_1) },
      /* Set the output type for Port B Pin 11 to as open drain. */
      { &GPIOB->OTYPER, ~(GPIO_OTYPER_ODR_11), (GPIO_OTYPER_ODR_11) },
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Exports the configuration table for the registers and the number of registers to be configured. */
const Port_ConfigType Port_gkt_Config =
   { Port_kat_Registers, PORT_NUMBER_OF_INIT_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

