/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the ADC module functions Adc_Init(), Adc_SetupResultBuffer(), Adc_StartGroupConversion(),
 *                Adc_GetGroupStatus().
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Array used to point to the variables that should hold the conversion result. */
static Adc_ValueGroupType *Adc_apt_BufferResults[ADC_NUMBER_OF_GROUPS];
/** \brief Local variable used to load the ADC group configuration. */
static Adc_GroupConfigType *Adc_pt_Group;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
/**
 * \brief      Initializes the ADC module using the register values from the configuration table.
 * \param      ConfigPtr: Pointer to the configuration table.
 * \return     -
 */
void Adc_Init(const Adc_ConfigType *ConfigPtr)
{
   Adc_pt_Group = (Adc_GroupConfigType*) (ConfigPtr->kpt_GroupConfig);
   RegInit_gv_Masked32Bits(ConfigPtr->kpt_InitConfig);
}


/**
 * \brief      Starts the conversion for the selected ADC group.
 * \param      Group: The ADC group to be converted.
 * \return     -
 */
void Adc_StartGroupConversion(Adc_GroupType Group)
{
   /* Set the first (and only) ADC channel number to be used in the regular sequence conversion. */
   Adc_pt_Group[Group].pt_Register->SQR3 = (uint32)(Adc_pt_Group[Group].t_Channel);
   /* Start the conversion. */
   Adc_pt_Group[Group].pt_Register->CR2 |= (ADC_CR2_SWSTART);
}

/**
 * \brief      Get the status of the conversion of the specified group.
 * \param      Group: The converted ADC group.
 * \return     The status of the conversion (IDLE/BUSY/COMPLETED/STREAM_COMPLETED).
 */
Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group)
{
   Adc_StatusType t_ReturnVal;
   if (((Adc_pt_Group[Group].pt_Register->SR) & (ADC_SR_STRT)) != 0U)
   {
      if (((Adc_pt_Group[Group].pt_Register->SR) & (ADC_SR_EOC)) != 0U)
      {
         t_ReturnVal = ADC_COMPLETED;
         /* Load the content of the data register into the variable set by SetupResultBuffer. */
         *Adc_apt_BufferResults[Group] = Adc_pt_Group[Group].pt_Register->DR;
         /* Reset the start bit from the status register. */
         Adc_pt_Group[Group].pt_Register->SR &= ~(ADC_SR_STRT);
      }
      else
      {
         t_ReturnVal = ADC_BUSY;
      }
   }
   else
   {
      t_ReturnVal = ADC_IDLE;
   }
   return t_ReturnVal;
}

/**
 * \brief      Initializes ADC driver with the group specific result buffer start address where the
 *             conversion results will be stored.
 * \param      Group: The converted ADC group.
 * \param      DataBufferPtr: Data buffer start address.
 * \return     E_OK - Buffer was setup correctly / E_NOT_OK - Setup failed.
 */
Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr)
{
   Adc_apt_BufferResults[Group] = (Adc_ValueGroupType*) DataBufferPtr;
   return E_OK;
}
