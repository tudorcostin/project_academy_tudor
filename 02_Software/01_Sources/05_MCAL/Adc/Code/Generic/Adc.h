/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Exports all the necessary data types and functions to be used by the ADC module.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef ADC_H
#define ADC_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Adc_Cfg.h"
#include "RegInit.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Define the number of ADC groups inside the group configuration table. */
#define ADC_NUMBER_OF_GROUPS (2U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Numeric ID of an ADC channel. */
typedef uint8 Adc_ChannelType;
/** \brief Numeric ID of an ADC channel group. */
typedef uint8 Adc_GroupType;
/** \brief Type for reading the converted values of a channel group. */
typedef uint16 Adc_ValueGroupType;
/** \brief Register init config type. */
typedef RegInit_Masked32BitsConfigType Adc_RegConfigType;
/** \brief Defines the group configuration type. */
typedef struct
{
   /** \brief Register to be interrogated/modified. */
   ADC_TypeDef* pt_Register;
   /** \brief Channel ID. */
   Adc_ChannelType t_Channel;
} Adc_GroupConfigType;

/** \brief  Defines the global ADC post build config. and group config. */
typedef struct
{
   /** \brief Pointer to the init configuration. */
   const RegInit_Masked32BitsConfigType *kpt_InitConfig;
   /** \brief Pointer to the group channel configuration. */
   const Adc_GroupConfigType *kpt_GroupConfig;
} Adc_ConfigType;

/** \brief Enum type defining the status of the ADC. */
typedef enum
{
   /** \brief Idle status value. */
   ADC_IDLE,
   /** \brief Busy status value. */
   ADC_BUSY,
   /** \brief Completed 1 conversion status value. */
   ADC_COMPLETED,
   /** \brief Stream Completed status value. */
   ADC_STREAM_COMPLETED,
} Adc_StatusType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Adc_ConfigType Adc_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Adc_Init(const Adc_ConfigType* ConfigPtr);
extern Std_ReturnType Adc_SetupResultBuffer(Adc_GroupType Group, const Adc_ValueGroupType* DataBufferPtr);
extern void Adc_StartGroupConversion(Adc_GroupType Group);
extern Adc_StatusType Adc_GetGroupStatus(Adc_GroupType Group);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* ADC_H */
