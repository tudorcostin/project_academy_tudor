/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Adc_PBcfg.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the configuration tables for the initialization of the 3 converters and the settings
 *                table for each ADC group implemented.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Adc.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Define the number of initialized registers in the configuration table.*/
#define ADC_NUMBER_OF_INIT_REGISTERS (10U)
/** \brief Define the resolution for the ADC conversions.*/
#define ADC_DRIVER_RESOLUTION_12BIT (0x00000000U)
/** \brief Define the ADCx->SQR1 register value for 1 conversion per channel. */
#define ADC_SQR1_CONVERSION_1 (0x00000000U)
/** \brief Define the ADC_CR2 register value for right data alignment. */
#define ADC_CR2_RIGHT_ALIGN (0x00000000U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Stores the initialization configuration for all the ADC registers.  */
static const RegInit_Masked32BitsSingleType Adc_kat_Registers[ADC_NUMBER_OF_INIT_REGISTERS] =
   {
      /* Start ADC1. */
      { &ADC1->CR2, ~(ADC_CR2_ADON), (ADC_CR2_ADON) },
      /* ADC1: Set number of clock cycles for port A channel 1 to 144.*/
      { &ADC1->SMPR2, ~(
      ADC_SMPR2_SMP1_1 |
      ADC_SMPR2_SMP1_2), (
      ADC_SMPR2_SMP1_1 |
      ADC_SMPR2_SMP1_2) },
      /* ADC1: Data alignment set to right. */
      { &ADC1->CR2, ~(ADC_CR2_RIGHT_ALIGN), (ADC_CR2_RIGHT_ALIGN) },
      /* ADC1: Set the conversion count to 1. */
      { &ADC1->SQR1, ~(ADC_SQR1_CONVERSION_1), (ADC_SQR1_CONVERSION_1) },
      /* ADC1: Set the resolution to 12bit. */
      { &ADC1->CR1, ~(ADC_DRIVER_RESOLUTION_12BIT), (ADC_DRIVER_RESOLUTION_12BIT) },
      /* Start ADC2. */
      { &ADC2->CR2, ~(ADC_CR2_ADON), ADC_CR2_ADON },
      /* ADC2: Data alignment set to right. */
      { &ADC2->CR2, ~(ADC_CR2_RIGHT_ALIGN), (ADC_CR2_RIGHT_ALIGN) },
      /* ADC2: Set the conversion count to 1. */
      { &ADC2->SQR1, ~(ADC_SQR1_CONVERSION_1), (ADC_SQR1_CONVERSION_1) },
      /* ADC2: Set the resolution to 12bit. */
      { &ADC2->CR1, ~(ADC_DRIVER_RESOLUTION_12BIT), (ADC_DRIVER_RESOLUTION_12BIT) },
      /* ADC2: Set number of clock cycles for port A channel 4 to 144.*/
      { &ADC2->SMPR2, ~(
      ADC_SMPR2_SMP4_1 |
      ADC_SMPR2_SMP4_2), (
      ADC_SMPR2_SMP4_1 |
      ADC_SMPR2_SMP4_2) },
   };

/** \brief Defines the configuration table for the registers and the number of registers to be configured. */
static const Adc_RegConfigType Adc_kt_InitConfig =
   { Adc_kat_Registers, ADC_NUMBER_OF_INIT_REGISTERS };

/** \brief Table containing the ADC group configuration. */
static const Adc_GroupConfigType Adc_kat_GroupConfig[ADC_NUMBER_OF_GROUPS] =
   {
      /*ADC_A_1 channel group configuration. */
      { ADC1, (Adc_ChannelType) (ADC_SQR3_SQ1_0) },
      /*ADC_A_4 channel group configuration. */
      { ADC2, (Adc_ChannelType) (ADC_SQR3_SQ1_2) },

   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Exports the configuration table for the init config and group config.*/
const Adc_ConfigType Adc_gkt_Config =
   { &Adc_kt_InitConfig, Adc_kat_GroupConfig };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

