/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the following interfaces Dio_ReadChannel(), Dio_WriteChannel(), Dio_ReadPort and
 *                Dio_WritePort().
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Dio.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Reads the level of the respective channel.
 * \param      ChannelId : The channel to be read.
 * \return     The level of the pin STD_LOW/STD_HIGH.
 */
Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId)
{
   Dio_LevelType t_ReturnVal;
   /* Load the level of the interrogated bit into an auxiliary variable. */
   uint32 ul_AuxVal = (Dio_gkt_ConfigTable[ChannelId].ul_Mask & Dio_gkt_ConfigTable[ChannelId].pt_Register->IDR);
   /* Test the variable to decide the return value. */
   if (ul_AuxVal > 0UL)
   {
      t_ReturnVal = STD_HIGH;
   }
   else
   {
      t_ReturnVal = STD_LOW;
   }
   return t_ReturnVal;
}

/**
 * \brief      Writes the desired level to the respective channel.
 * \param      ChannelId : The channel to be written.
 * \param      Level : The level to be loaded on the channel (Values: STD_HIGH/STD_LOW)
 * \return     -
 */
void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level)
{
   if (Level == STD_HIGH)
   {
      Dio_gkt_ConfigTable[ChannelId].pt_Register->ODR |= Dio_gkt_ConfigTable[ChannelId].ul_Mask;
   }
   else
   {
      Dio_gkt_ConfigTable[ChannelId].pt_Register->ODR &= ~Dio_gkt_ConfigTable[ChannelId].ul_Mask;
   }
}

/**
 * \brief      Reads the level of the respective port.
 * \param      PortId : The port to be read.
 * \return     The level of the port.
 */
Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId)
{
   return (Dio_PortLevelType) Dio_gkt_ConfigTable[PortId].pt_Register->IDR;
}

/**
 * \brief      Writes the desired level to the specified port.
 * \param      PortId : The port to be written.
 * \param      Level : The level to be loaded on the port.
 * \return     -
 */
void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level)
{
   Dio_gkt_ConfigTable[PortId].pt_Register->ODR = (uint32) Level;
}

