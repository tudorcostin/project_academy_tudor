/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Exports all the necessary data types and the implemented interfaces.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_H
#define DIO_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "Dio_Cfg.h"
#include "stm32f407xx.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Defines the Dio configuration type. */
typedef struct
{
   /** \brief Register to be interrogated/modified.*/
   GPIO_TypeDef* pt_Register;
   /** \brief Mask to be applied.*/
   uint32 ul_Mask;
} Dio_ConfigType;
/** \brief Data type that defines the possible levels that a DIO channel can have(STD_LOW/STD_HIGH). */
typedef uint8 Dio_LevelType;
/** \brief Data type that defines the values that a DIO port can have. */
typedef uint16 Dio_PortLevelType;
/** \brief Data type that defines the numeric ID of a DIO channel. */
typedef uint8 Dio_ChannelType;
/** \brief Data type that defines the numeric ID of a DIO port. */
typedef uint8 Dio_PortType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Dio_ConfigType Dio_gkt_ConfigTable[DIO_NUMBER_OF_CHANNELS];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern Dio_LevelType Dio_ReadChannel(Dio_ChannelType ChannelId);
extern void Dio_WriteChannel(Dio_ChannelType ChannelId, Dio_LevelType Level);
extern Dio_PortLevelType Dio_ReadPort(Dio_PortType PortId);
extern void Dio_WritePort(Dio_PortType PortId, Dio_PortLevelType Level);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_H */
