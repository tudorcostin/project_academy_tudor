/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio_Cfg.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Defines the identifiers of the channels used in the configuration table.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_CFG_H
#define DIO_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Number of channels for the configuration table. */
#define DIO_NUMBER_OF_CHANNELS (21U)
/** \brief Index for port A */
#define DIO_A (0U)
/** \brief Index for port C */
#define DIO_C (3U)
/** \brief Index for port D */
#define DIO_D (10U)
/** \brief Index for port D */
#define DIO_E (14U)

/** \brief Index for port A channel 1 in the configuration table. */
#define DIO_A_0 (0U)
/** \brief Index for port A channel 1 in the configuration table. */
#define DIO_A_2 (1U)
/** \brief Index for port A channel 7 in the configuration table. */
#define DIO_A_7 (2U)

/** \brief Index for port C channel 6 in the configuration table. */
#define DIO_C_6 (3U)
/** \brief Index for port C channel 7 in the configuration table. */
#define DIO_C_7 (4U)
/** \brief Index for port C channel 8 in the configuration table. */
#define DIO_C_8 (5U)
/** \brief Index for port C channel 9 in the configuration table. */
#define DIO_C_9 (6U)
/** \brief Index for port C channel 10 in the configuration table. */
#define DIO_C_10 (7U)
/** \brief Index for port C channel 11 in the configuration table. */
#define DIO_C_11 (8U)
/** \brief Index for port C channel 12 in the configuration table. */
#define DIO_C_12 (9U)

/** \brief Index for port D channel 8 in the configuration table. */
#define DIO_D_8 (10U)
/** \brief Index for port D channel 9 in the configuration table. */
#define DIO_D_9 (11U)
/** \brief Index for port D channel 10 in the configuration table. */
#define DIO_D_10 (12U)
/** \brief Index for port D channel 11 in the configuration table. */
#define DIO_D_11 (13U)

/** \brief Index for port E channel 7 in the configuration table. */
#define DIO_E_7 (14U)
/** \brief Index for port E channel 8 in the configuration table. */
#define DIO_E_8 (15U)
/** \brief Index for port E channel 9 in the configuration table. */
#define DIO_E_9 (16U)
/** \brief Index for port E channel 10 in the configuration table. */
#define DIO_E_10 (17U)
/** \brief Index for port E channel 11 in the configuration table. */
#define DIO_E_11 (18U)
/** \brief Index for port E channel 12 in the configuration table. */
#define DIO_E_12 (19U)
/** \brief Index for port E channel 13 in the configuration table. */
#define DIO_E_13 (20U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_CFG_H */
