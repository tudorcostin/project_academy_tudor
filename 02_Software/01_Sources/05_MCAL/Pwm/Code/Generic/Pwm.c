/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm.c
 *    \author     Gadei Costin-Tudor
 *    \brief      Implements the Pwm_Init() and Pwm_SetDutyCycle() functions.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Pwm.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Local variable used to store the PWM channel configuration. */
static Pwm_ChannelConfigType* Pwm_pt_ChannelConfig;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes the PWM module using the register values from the configuration table.
 * \param      ConfigPtr: Pointer to the global configuration table.
 * \return     -
 */
void Pwm_Init(const Pwm_ConfigType* ConfigPtr)
{
   Pwm_pt_ChannelConfig = (Pwm_ChannelConfigType*) ConfigPtr->kpt_ChannelConfig;
   RegInit_gv_Masked32Bits(ConfigPtr->kpt_InitConfig);
}

/**
 * \brief      Sets the duty cycle of the PWM channel.
 * \param      ChannelNumber: Index for the channel configuration table.
 * \param      DutyCycle: PWM Duty cycle value (Min = 0x0000; Max = 0x8000).
 * \return     -
 */
void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber, uint16 DutyCycle)
{
   *Pwm_pt_ChannelConfig[ChannelNumber].pt_DutyConfig = (((*Pwm_pt_ChannelConfig[ChannelNumber].pt_PeriodReg + 1)
      * DutyCycle) >> 15);
}
