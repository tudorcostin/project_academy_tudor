/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm.h
 *    \author     Gadei Costin-Tudor
 *    \brief      Exports all the necessary data types and functions to be used by the PWM module.
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef PWM_H
#define PWM_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"
#include "RegInit.h"
#include "stm32f407xx.h"
#include "Pwm_Cfg.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief PWM Channel ID data type. */
typedef uint8 Pwm_ChannelType;

/** \brief PWM Channel configuration type.*/
typedef struct
{
   /** \brief PWM Frequency.*/
   volatile uint32 *pt_PeriodReg;
   /** \brief PWM Duty cycle.*/
   volatile uint32 *pt_DutyConfig;
} Pwm_ChannelConfigType;

/** \brief PWM Global configuration type.*/
typedef struct
{
   /** \brief Pointer to the initialization config. table. */
   const RegInit_Masked32BitsConfigType *kpt_InitConfig;
   /** \brief Pointer to the channel config. table. */
   const Pwm_ChannelConfigType *kpt_ChannelConfig;
} Pwm_ConfigType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const Pwm_ConfigType Pwm_gkt_Config;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Pwm_Init(const Pwm_ConfigType* ConfigPtr);
extern void Pwm_SetDutyCycle(Pwm_ChannelType ChannelNumber, uint16 DutyCycle);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* PWM_H */
