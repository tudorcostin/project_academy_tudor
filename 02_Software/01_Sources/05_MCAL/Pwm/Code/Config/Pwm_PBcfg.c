/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm_PBcfg.c
 *    \author     Gadei Costin-Tudor
 *    \brief
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pwm.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Defines the number of registers in the initialization look-up table. */
#define PWM_NUMBER_OF_INIT_REGISTERS (14U)
/** \brief Defines the number of PWM channels. */
#define PWM_NUMBER_OF_CHANNELS (2U)
/** \brief Defines the Prescale factor of 3 for dividing the clock frequency to 4. */
#define PWM_PRESCALE_3 (39U)
/** \brief Defines the ARR value corresponding to a PWM frequency of 0,5KHz. */
#define PWM_ARR_50HZ (40000U)
/** \brief Defines the ARR value corresponding to a PWM frequency of 1KHz. */
#define PWM_ARR_1KHZ (5000U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Stores the initialization configuration for the Timer / PWM registers. */
static const RegInit_Masked32BitsSingleType Pwm_kat_InitConfig[PWM_NUMBER_OF_INIT_REGISTERS] =
   {
      /* PWM_A_1 Port B Channel 3 - Timer 2 Channel 2 Configuration*/
      /* Timer2, Channel 2: Frequency = 20MHz */
      {
         &TIM2->PSC, ~(
         PWM_PRESCALE_3), (
         PWM_PRESCALE_3)
      },
      /* Timer2, Channel 2: PWM Frequency = 0.5 KHz */
      {
         &TIM2->ARR, ~(
         0xFFFFFFFFU), (
         PWM_ARR_50HZ)
      },
      /* Timer2, Channel 2: Counter enable. */
      {
         &TIM2->CR1, ~(
         TIM_CR1_CEN), (
         TIM_CR1_CEN)
      },
      /* Timer 2, Channel 2: Capture/Compare enabled.*/
      {
         &TIM2->CCER, ~(
         TIM_CCER_CC2E), (
         TIM_CCER_CC2E)
      },
      /* Timer 2, Channel 2: Output Compare mode set to PWM Mode 1. */
      {
         &TIM2->CCMR1, ~(
         TIM_CCMR1_OC2M_1 |
         TIM_CCMR1_OC2M_2), (
         TIM_CCMR1_OC2M_1 |
         TIM_CCMR1_OC2M_2)
      },
      /* Timer 2 Channel 2: Output compare preload enabled. */
      {
         &TIM2->CCMR1, ~(
         TIM_CCMR1_OC2PE), (
         TIM_CCMR1_OC2PE)
      },
      /* Timer 2, Channel 2: Update generation. */
      {
         &TIM2->EGR, ~(
         TIM_EGR_UG), (
         TIM_EGR_UG)
      },

      /* PWM_B_5 Port B Channel 5 - Timer 3 Channel 2 Configuration */
      /* Timer, 3 Channel 2: Frequency = 20MHz */
      {
         &TIM3->PSC, ~(
         PWM_PRESCALE_3), (
         PWM_PRESCALE_3)
      },
      /* Timer, 3 Channel 2: PWM Frequency = 1KHz */
      {
         &TIM3->ARR, ~(
         TIM_ARR_ARR), (
         PWM_ARR_1KHZ)
      },
      /* Timer3, Channel 2: Counter enable. */
      {
         &TIM3->CR1, ~(
         TIM_CR1_CEN), (
         TIM_CR1_CEN)
      },
      /* Timer 3, Channel 2: Capture/Compare enabled.*/
      {
         &TIM3->CCER, ~(
         TIM_CCER_CC2E), (
         TIM_CCER_CC2E)
      },
      /* Timer 3, Channel 2: Output Compare mode set to PWM Mode 1. */
      {
         &TIM3->CCMR1, ~(
         TIM_CCMR1_OC2M_1 |
         TIM_CCMR1_OC2M_2), (
         TIM_CCMR1_OC2M_1 |
         TIM_CCMR1_OC2M_2)
      },
      /* Timer 3 Channel 2: Output compare preload enabled. */
      {
         &TIM3->CCMR1, ~(
         TIM_CCMR1_OC2PE), (
         TIM_CCMR1_OC2PE)
      },
      /* Timer 3, Channel 2: Update generation. */
      {
         &TIM3->EGR, ~(
         TIM_EGR_UG), (
         TIM_EGR_UG)
      },
   };
/** \brief Defines the configuration table for the registers and the number of registers to be configured. */
static const RegInit_Masked32BitsConfigType Pwm_kpt_InitConfig =
   { Pwm_kat_InitConfig, PWM_NUMBER_OF_INIT_REGISTERS };

/** \brief  Table containing the PWM Channel configuration. */
static const Pwm_ChannelConfigType Pwm_kat_Channels[PWM_NUMBER_OF_CHANNELS] =
   {
      /* PWM_A_1: Timer 2 Channel 2 */
      { (uint32*) &TIM2->ARR, (uint32*) &TIM2->CCR2 },
      /* PWM_B_5: Timer 3 Channel 2 */
      { (uint32*) &TIM3->ARR, (uint32*) &TIM3->CCR2 },
   };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief Exports the global initialization table. */
const Pwm_ConfigType Pwm_gkt_Config =
   { &Pwm_kpt_InitConfig, Pwm_kat_Channels };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

