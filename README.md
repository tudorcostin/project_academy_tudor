# Academy Framework

Contains a basic AUTOSAR implementation of the ECU manager, MCU driver, Operating System, RTE and a series of source code templates, including examples of configuring software components and the RTE.

This framework does not contain implementations for sensor or actuator drivers nor for any software components and is the basis for all the Embedded Academy projects.